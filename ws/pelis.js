var numPage = 1;

/*
  JSONPACK
  Copyright (c) 2013, Rodrigo González, Sapienlab All Rights Reserved.
  Available via MIT LICENSE. See https://github.com/roro89/jsonpack/blob/master/LICENSE.md for details.
 */
  (function(define) {

    define([], function() {
  
      var TOKEN_TRUE = -1;
      var TOKEN_FALSE = -2;
      var TOKEN_NULL = -3;
      var TOKEN_EMPTY_STRING = -4;
      var TOKEN_UNDEFINED = -5;
  
      var pack = function(json, options) {
  
        // Canonizes the options
        options = options || {};
  
        // A shorthand for debugging
        var verbose = options.verbose || false;
  
        verbose && console.log('Normalize the JSON Object');
  
        // JSON as Javascript Object (Not string representation)
        json = typeof json === 'string' ? this.JSON.parse(json) : json;
  
        verbose && console.log('Creating a empty dictionary');
  
        // The dictionary
        var dictionary = {
          strings : [],
          integers : [],
          floats : []
        };
  
        verbose && console.log('Creating the AST');
  
        // The AST
        var ast = (function recursiveAstBuilder(item) {
  
          verbose && console.log('Calling recursiveAstBuilder with ' + this.JSON.stringify(item));
  
          // The type of the item
          var type = typeof item;
  
          // Case 7: The item is null
          if (item === null) {
            return {
              type : 'null',
              index : TOKEN_NULL
            };
          }
          
          //add undefined 
          if (typeof item === 'undefined') {
            return {
              type : 'undefined',
              index : TOKEN_UNDEFINED
            };
          }
  
          // Case 1: The item is Array Object
          if ( item instanceof Array) {
  
            // Create a new sub-AST of type Array (@)
            var ast = ['@'];
  
            // Add each items
            for (var i in item) {
              
              if (!item.hasOwnProperty(i)) continue;
  
              ast.push(recursiveAstBuilder(item[i]));
            }
  
            // And return
            return ast;
  
          }
  
          // Case 2: The item is Object
          if (type === 'object') {
  
            // Create a new sub-AST of type Object ($)
            var ast = ['$'];
  
            // Add each items
            for (var key in item) {
  
              if (!item.hasOwnProperty(key))
                continue;
  
              ast.push(recursiveAstBuilder(key));
              ast.push(recursiveAstBuilder(item[key]));
            }
  
            // And return
            return ast;
  
          }
  
          // Case 3: The item empty string
          if (item === '') {
            return {
              type : 'empty',
              index : TOKEN_EMPTY_STRING
            };
          }
  
          // Case 4: The item is String
          if (type === 'string') {
  
            // The index of that word in the dictionary
            var index = _indexOf.call(dictionary.strings, item);
  
            // If not, add to the dictionary and actualize the index
            if (index == -1) {
              dictionary.strings.push(_encode(item));
              index = dictionary.strings.length - 1;
            }
  
            // Return the token
            return {
              type : 'strings',
              index : index
            };
          }
  
          // Case 5: The item is integer
          if (type === 'number' && item % 1 === 0) {
  
            // The index of that number in the dictionary
            var index = _indexOf.call(dictionary.integers, item);
  
            // If not, add to the dictionary and actualize the index
            if (index == -1) {
              dictionary.integers.push(_base10To36(item));
              index = dictionary.integers.length - 1;
            }
  
            // Return the token
            return {
              type : 'integers',
              index : index
            };
          }
  
          // Case 6: The item is float
          if (type === 'number') {
            // The index of that number in the dictionary
            var index = _indexOf.call(dictionary.floats, item);
  
            // If not, add to the dictionary and actualize the index
            if (index == -1) {
              // Float not use base 36
              dictionary.floats.push(item);
              index = dictionary.floats.length - 1;
            }
  
            // Return the token
            return {
              type : 'floats',
              index : index
            };
          }
  
          // Case 7: The item is boolean
          if (type === 'boolean') {
            return {
              type : 'boolean',
              index : item ? TOKEN_TRUE : TOKEN_FALSE
            };
          }
  
          // Default
          throw new Error('Unexpected argument of type ' + typeof (item));
  
        })(json);
  
        // A set of shorthands proxies for the length of the dictionaries
        var stringLength = dictionary.strings.length;
        var integerLength = dictionary.integers.length;
        var floatLength = dictionary.floats.length;
  
        verbose && console.log('Parsing the dictionary');
  
        // Create a raw dictionary
        var packed = dictionary.strings.join('|');
        packed += '^' + dictionary.integers.join('|');
        packed += '^' + dictionary.floats.join('|');
  
        verbose && console.log('Parsing the structure');
  
        // And add the structure
        packed += '^' + (function recursiveParser(item) {
  
          verbose && console.log('Calling a recursiveParser with ' + this.JSON.stringify(item));
  
          // If the item is Array, then is a object of
          // type [object Object] or [object Array]
          if ( item instanceof Array) {
  
            // The packed resulting
            var packed = item.shift();
  
            for (var i in item) {
              
              if (!item.hasOwnProperty(i)) 
                continue;
              
              packed += recursiveParser(item[i]) + '|';
            }
  
            return (packed[packed.length - 1] === '|' ? packed.slice(0, -1) : packed) + ']';
  
          }
  
          // A shorthand proxies
          var type = item.type, index = item.index;
  
          if (type === 'strings') {
            // Just return the base 36 of index
            return _base10To36(index);
          }
  
          if (type === 'integers') {
            // Return a base 36 of index plus stringLength offset
            return _base10To36(stringLength + index);
          }
  
          if (type === 'floats') {
            // Return a base 36 of index plus stringLength and integerLength offset
            return _base10To36(stringLength + integerLength + index);
          }
  
          if (type === 'boolean') {
            return item.index;
          }
  
          if (type === 'null') {
            return TOKEN_NULL;
          }
  
          if (type === 'undefined') {
            return TOKEN_UNDEFINED;
          }
  
          if (type === 'empty') {
            return TOKEN_EMPTY_STRING;
          }
  
          throw new TypeError('The item is alien!');
  
        })(ast);
  
        verbose && console.log('Ending parser');
  
        // If debug, return a internal representation of dictionary and stuff
        if (options.debug)
          return {
            dictionary : dictionary,
            ast : ast,
            packed : packed
          };
  
        return packed;
  
      };
  
      var unpack = function(packed, options) {
  
        // Canonizes the options
        options = options || {};
  
        // A raw buffer
        var rawBuffers = packed.split('^');
  
        // Create a dictionary
        options.verbose && console.log('Building dictionary');
        var dictionary = [];
  
        // Add the strings values
        var buffer = rawBuffers[0];
        if (buffer !== '') {
          buffer = buffer.split('|');
          options.verbose && console.log('Parse the strings dictionary');
          for (var i=0, n=buffer.length; i<n; i++){
            dictionary.push(_decode(buffer[i]));
          }
        }
  
        // Add the integers values
        buffer = rawBuffers[1];
        if (buffer !== '') {
          buffer = buffer.split('|');
          options.verbose && console.log('Parse the integers dictionary');
          for (var i=0, n=buffer.length; i<n; i++){
            dictionary.push(_base36To10(buffer[i]));
          }
        }
  
        // Add the floats values
        buffer = rawBuffers[2];
        if (buffer !== '') {
          buffer = buffer.split('|')
          options.verbose && console.log('Parse the floats dictionary');
          for (var i=0, n=buffer.length; i<n; i++){
            dictionary.push(parseFloat(buffer[i]));
          }
        }
        // Free memory
        buffer = null;
  
        options.verbose && console.log('Tokenizing the structure');
  
        // Tokenizer the structure
        var number36 = '';
        var tokens = [];
        var len=rawBuffers[3].length;
        for (var i = 0; i < len; i++) {
          var symbol = rawBuffers[3].charAt(i);
          if (symbol === '|' || symbol === '$' || symbol === '@' || symbol === ']') {
            if (number36) {
              tokens.push(_base36To10(number36));
              number36 = '';
            }
            symbol !== '|' && tokens.push(symbol);
          } else {
            number36 += symbol;
          }
        }
  
        // A shorthand proxy for tokens.length
        var tokensLength = tokens.length;
  
        // The index of the next token to read
        var tokensIndex = 0;
  
        options.verbose && console.log('Starting recursive parser');
  
        return (function recursiveUnpackerParser() {
  
          // Maybe '$' (object) or '@' (array)
          var type = tokens[tokensIndex++];
  
          options.verbose && console.log('Reading collection type ' + (type === '$' ? 'object' : 'Array'));
  
          // Parse an array
          if (type === '@') {
  
            var node = [];
  
            for (; tokensIndex < tokensLength; tokensIndex++) {
              var value = tokens[tokensIndex];
              options.verbose && console.log('Read ' + value + ' symbol');
              if (value === ']')
                return node;
              if (value === '@' || value === '$') {
                node.push(recursiveUnpackerParser());
              } else {
                switch(value) {
                  case TOKEN_TRUE:
                    node.push(true);
                    break;
                  case TOKEN_FALSE:
                    node.push(false);
                    break;
                  case TOKEN_NULL:
                    node.push(null);
                    break;
                  case TOKEN_UNDEFINED:
                    node.push(undefined);
                    break;
                  case TOKEN_EMPTY_STRING:
                    node.push('');
                    break;
                  default:
                    node.push(dictionary[value]);
                }
  
              }
            }
  
            options.verbose && console.log('Parsed ' + this.JSON.stringify(node));
  
            return node;
  
          }
  
          // Parse a object
          if (type === '$') {
            var node = {};
  
            for (; tokensIndex < tokensLength; tokensIndex++) {
  
              var key = tokens[tokensIndex];
  
              if (key === ']')
                return node;
  
              if (key === TOKEN_EMPTY_STRING)
                key = '';
              else
                key = dictionary[key];
  
              var value = tokens[++tokensIndex];
  
              if (value === '@' || value === '$') {
                node[key] = recursiveUnpackerParser();
              } else {
                switch(value) {
                  case TOKEN_TRUE:
                    node[key] = true;
                    break;
                  case TOKEN_FALSE:
                    node[key] = false;
                    break;
                  case TOKEN_NULL:
                    node[key] = null;
                    break;
                  case TOKEN_UNDEFINED:
                    node[key] = undefined;
                    break;
                  case TOKEN_EMPTY_STRING:
                    node[key] = '';
                    break;
                  default:
                    node[key] = dictionary[value];
                }
  
              }
            }
  
            options.verbose && console.log('Parsed ' + this.JSON.stringify(node));
  
            return node;
          }
  
          throw new TypeError('Bad token ' + type + ' isn\'t a type');
  
        })();
  
      }
      /**
       * Get the index value of the dictionary
       * @param {Object} dictionary a object that have two array attributes: 'string' and 'number'
       * @param {Object} data
       */
      var _indexOfDictionary = function(dictionary, value) {
  
        // The type of the value
        var type = typeof value;
  
        // If is boolean, return a boolean token
        if (type === 'boolean')
          return value ? TOKEN_TRUE : TOKEN_FALSE;
  
        // If is null, return a... yes! the null token
        if (value === null)
          return TOKEN_NULL;
  
        //add undefined
        if (typeof value === 'undefined')
          return TOKEN_UNDEFINED;
  
  
        if (value === '') {
          return TOKEN_EMPTY_STRING;
        }
  
        if (type === 'string') {
          value = _encode(value);
          var index = _indexOf.call(dictionary.strings, value);
          if (index === -1) {
            dictionary.strings.push(value);
            index = dictionary.strings.length - 1;
          }
        }
  
        // If has an invalid JSON type (example a function)
        if (type !== 'string' && type !== 'number') {
          throw new Error('The type is not a JSON type');
        };
  
        if (type === 'string') {// string
          value = _encode(value);
        } else if (value % 1 === 0) {// integer
          value = _base10To36(value);
        } else {// float
  
        }
  
        // If is number, "serialize" the value
        value = type === 'number' ? _base10To36(value) : _encode(value);
  
        // Retrieve the index of that value in the dictionary
        var index = _indexOf.call(dictionary[type], value);
  
        // If that value is not in the dictionary
        if (index === -1) {
          // Push the value
          dictionary[type].push(value);
          // And return their index
          index = dictionary[type].length - 1;
        }
  
        // If the type is a number, then add the '+'  prefix character
        // to differentiate that they is a number index. If not, then
        // just return a 36-based representation of the index
        return type === 'number' ? '+' + index : index;
  
      };
  
      var _encode = function(str) {
        if ( typeof str !== 'string')
          return str;
  
        return str.replace(/[\+ \|\^\%]/g, function(a) {
          return ({
          ' ' : '+',
          '+' : '%2B',
          '|' : '%7C',
          '^' : '%5E',
          '%' : '%25'
          })[a]
        });
      };
  
      var _decode = function(str) {
        if ( typeof str !== 'string')
          return str;
  
        return str.replace(/\+|%2B|%7C|%5E|%25/g, function(a) {
          return ({
          '+' : ' ',
          '%2B' : '+',
          '%7C' : '|',
          '%5E' : '^',
          '%25' : '%'
          })[a]
        })
      };
  
      var _base10To36 = function(number) {
        return Number.prototype.toString.call(number, 36).toUpperCase();
      };
  
      var _base36To10 = function(number) {
        return parseInt(number, 36);
      };
  
      var _indexOf = Array.prototype.indexOf ||
      function(obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
          if (this[i] === obj) {
            return i;
          }
        }
        return -1;
      };
  
      return {
        JSON : JSON,
        pack : pack,
        unpack : unpack
      };
  
    });
  
  })( typeof define == 'undefined' || !define.amd ? function(deps, factory) {
    var jsonpack = factory();
    if ( typeof exports != 'undefined')
      for (var key in jsonpack)
      exports[key] = jsonpack[key];
    else
      window.jsonpack = jsonpack;
  } : define);

const pelisCompressed = `rank|title|The+Shawshank+Redemption|thumbnail|https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UY67_CR0,0,45,67_AL_.jpg|rating|9.3|id|top1|year|image|https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_QL75_UX380_CR0,1,380,562_.jpg|description|Two+imprisoned+men+bond+over+a+number+of+years,+finding+solace+and+eventual+redemption+through+acts+of+common+decency.|trailer|https://www.youtube.com/embed/NmzuHjWmXOc|genre|Drama|director|Frank+Darabont|writers|Stephen+King+(based+on+the+short+novel+"Rita+Hayworth+and+the+Shawshank+Redemption"+by)|Frank+Darabont+(screenplay+by)|imdbid|tt0111161|The+Godfather|https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR1,0,45,67_AL_.jpg|9.2|top2|https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UY562_CR8,0,380,562_.jpg|The+aging+patriarch+of+an+organized+crime+dynasty+in+postwar+New+York+City+transfers+control+of+his+clandestine+empire+to+his+reluctant+youngest+son.|https://www.youtube.com/embed/rqGJyUB1Q3s|Crime|Francis+Ford+Coppola|Mario+Puzo+(screenplay+by)|Francis+Ford+Coppola+(screenplay+by)|tt0068646|The+Dark+Knight|https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX67_CR0,0,67,98_AL_.jpg|9.0|top3|https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_QL75_UX380_CR0,0,380,562_.jpg|When+the+menace+known+as+the+Joker+wreaks+havoc+and+chaos+on+the+people+of+Gotham,+Batman+must+accept+one+of+the+greatest+psychological+and+physical+tests+of+his+ability+to+fight+injustice.|https://www.youtube.com/embed/EXeTwQWrcwY|Action|Christopher+Nolan|Jonathan+Nolan+(screenplay)|Christopher+Nolan+(screenplay)|David+S.+Goyer+(story)|tt0468569|The+Godfather+Part+II|https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR1,0,45,67_AL_.jpg|top4|https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UY562_CR7,0,380,562_.jpg|The+early+life+and+career+of+Vito+Corleone+in+1920s+New+York+City+is+portrayed,+while+his+son,+Michael,+expands+and+tightens+his+grip+on+the+family+crime+syndicate.|https://www.youtube.com/embed/9O1Iy9od7-A|Francis+Ford+Coppola|Francis+Ford+Coppola+(screenplay+by)|Mario+Puzo+(screenplay+by)|tt0071562|12+Angry+Men|https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UX45_CR0,0,45,67_AL_.jpg|top5|https://m.media-amazon.com/images/M/MV5BMWU4N2FjNzYtNTVkNC00NzQ0LTg0MjAtYTJlMjFhNGUxZDFmXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_QL75_UX380_CR0,11,380,562_.jpg|The+jury+in+a+New+York+City+murder+trial+is+frustrated+by+a+single+member+whose+skeptical+caution+forces+them+to+more+carefully+consider+the+evidence+before+jumping+to+a+hasty+verdict.|https://www.youtube.com/embed/TEN-2uTi2c0|Sidney+Lumet|Reginald+Rose+(teleplay+"Twelve+Angry+Men")|tt0050083|Schindler's+List|https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|8.9|top6|https://m.media-amazon.com/images/M/MV5BNDE4OTMxMTctNmRhYy00NWE2LTg3YzItYTk3M2UwOTU5Njg4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,4,380,562_.jpg|In+German-occupied+Poland+during+World+War+II,+industrialist+Oskar+Schindler+gradually+becomes+concerned+for+his+Jewish+workforce+after+witnessing+their+persecution+by+the+Nazis.|https://www.youtube.com/embed/gG22XNhtnoY|Biography|History|Steven+Spielberg|Thomas+Keneally+(book)|Steven+Zaillian+(screenplay)|tt0108052|The+Lord+of+the+Rings:+The+Return+of+the+King|https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR0,0,45,67_AL_.jpg|top7|https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,0,380,562_.jpg|Gandalf+and+Aragorn+lead+the+World+of+Men+against+Sauron's+army+to+draw+his+gaze+from+Frodo+and+Sam+as+they+approach+Mount+Doom+with+the+One+Ring.|https://www.youtube.com/embed/y2rYRu8UW8M|Adventure|Peter+Jackson|J.R.R.+Tolkien+(novel+"The+Return+of+the+King")|Fran+Walsh+(screenplay)|Philippa+Boyens+(screenplay)|tt0167260|Pulp+Fiction|https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR0,0,45,67_AL_.jpg|8.8|top8|https://m.media-amazon.com/images/M/MV5BNGNhMDIzZTUtNTBlZi00MTRlLWFjM2ItYzViMjE3YzI5MjljXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UY562_CR3,0,380,562_.jpg|The+lives+of+two+mob+hitmen,+a+boxer,+a+gangster+and+his+wife,+and+a+pair+of+diner+bandits+intertwine+in+four+tales+of+violence+and+redemption.|https://www.youtube.com/embed/5ZAhzsi1ybM|Quentin+Tarantino|Quentin+Tarantino+(stories+by)|Roger+Avary+(stories+by)|tt0110912|The+Lord+of+the+Rings:+The+Fellowship+of+the+Ring|https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_UY67_CR0,0,45,67_AL_.jpg|top9|https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_QL75_UX380_CR0,1,380,562_.jpg|A+meek+Hobbit+from+the+Shire+and+eight+companions+set+out+on+a+journey+to+destroy+the+powerful+One+Ring+and+save+Middle-earth+from+the+Dark+Lord+Sauron.|https://www.youtube.com/embed/_e8QGuG50ro|Peter+Jackson|J.R.R.+Tolkien+(novel+"The+Fellowship+of+the+Ring")|Fran+Walsh+(screenplay)|Philippa+Boyens+(screenplay)|tt0120737|The+Good,+the+Bad+and+the+Ugly|https://m.media-amazon.com/images/M/MV5BNjJlYmNkZGItM2NhYy00MjlmLTk5NmQtNjg1NmM2ODU4OTMwXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UX45_CR0,0,45,67_AL_.jpg|top10|https://m.media-amazon.com/images/M/MV5BNjJlYmNkZGItM2NhYy00MjlmLTk5NmQtNjg1NmM2ODU4OTMwXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,4,380,562_.jpg|A+bounty+hunting+scam+joins+two+men+in+an+uneasy+alliance+against+a+third+in+a+race+to+find+a+fortune+in+gold+buried+in+a+remote+cemetery.|https://www.youtube.com/embed/WCN5JJY_wiA|Western|Sergio+Leone|Luciano+Vincenzoni+(story)|Sergio+Leone+(story)|Agenore+Incrocci+(screenplay)|tt0060196|Forrest+Gump|https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UY67_CR0,0,45,67_AL_.jpg|top11|https://m.media-amazon.com/images/M/MV5BNWIwODRlZTUtY2U3ZS00Yzg1LWJhNzYtMmZiYmEyNmU1NjMzXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UY562_CR4,0,380,562_.jpg|The+presidencies+of+Kennedy+and+Johnson,+the+Vietnam+War,+the+Watergate+scandal+and+other+historical+events+unfold+from+the+perspective+of+an+Alabama+man+with+an+IQ+of+75,+whose+only+desire+is+to+be+reunited+with+his+childhood+sweetheart.|https://www.youtube.com/embed/bLvqoHBptjg|Romance|Robert+Zemeckis|Winston+Groom+(novel)|Eric+Roth+(screenplay)|tt0109830|Fight+Club|https://m.media-amazon.com/images/M/MV5BNDIzNDU0YzEtYzE5Ni00ZjlkLTk5ZjgtNjM3NWE4YzA3Nzk3XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UY67_CR0,0,45,67_AL_.jpg|8.7|top12|https://m.media-amazon.com/images/M/MV5BNDIzNDU0YzEtYzE5Ni00ZjlkLTk5ZjgtNjM3NWE4YzA3Nzk3XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,1,380,562_.jpg|An+insomniac+office+worker+and+a+devil-may-care+soap+maker+form+an+underground+fight+club+that+evolves+into+much+more.|https://www.youtube.com/embed/qtRKdVHc-cE|David+Fincher|Chuck+Palahniuk+(based+on+the+novel+by)|Jim+Uhls+(screenplay+by)|tt0137523|Inception|https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_UY67_CR0,0,45,67_AL_.jpg|top13|https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+thief+who+steals+corporate+secrets+through+the+use+of+dream-sharing+technology+is+given+the+inverse+task+of+planting+an+idea+into+the+mind+of+a+C.E.O.,+but+his+tragic+past+may+doom+the+project+and+his+team+to+disaster.|https://www.youtube.com/embed/YoHD9XEInc0|Sci-Fi|Christopher+Nolan|Christopher+Nolan|tt1375666|The+Lord+of+the+Rings:+The+Two+Towers|https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|top14|https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,14,380,562_.jpg|While+Frodo+and+Sam+edge+closer+to+Mordor+with+the+help+of+the+shifty+Gollum,+the+divided+fellowship+makes+a+stand+against+Sauron's+new+ally,+Saruman,+and+his+hordes+of+Isengard.|https://www.youtube.com/embed/hYcw5ksV8YQ|Peter+Jackson|J.R.R.+Tolkien+(novel+"The+Two+Towers")|Fran+Walsh+(screenplay)|Philippa+Boyens+(screenplay)|tt0167261|Star+Wars:+Episode+V+-+The+Empire+Strikes+Back|https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top15|https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,15,380,562_.jpg|After+the+Rebels+are+brutally+overpowered+by+the+Empire+on+the+ice+planet+Hoth,+Luke+Skywalker+begins+Jedi+training+with+Yoda,+while+his+friends+are+pursued+across+the+galaxy+by+Darth+Vader+and+bounty+hunter+Boba+Fett.|https://www.youtube.com/embed/JNwNXF9Y6kY|Fantasy|Irvin+Kershner|Leigh+Brackett+(screenplay+by)|Lawrence+Kasdan+(screenplay+by)|George+Lucas+(story+by)|tt0080684|The+Matrix|https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|top16|https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,4,380,562_.jpg|When+a+beautiful+stranger+leads+computer+hacker+Neo+to+a+forbidding+underworld,+he+discovers+the+shocking+truth--the+life+he+knows+is+the+elaborate+deception+of+an+evil+cyber-intelligence.|https://www.youtube.com/embed/vKQi3bBA1y8|Lana+Wachowski|Lilly+Wachowski|Lilly+Wachowski|Lana+Wachowski|tt0133093|Goodfellas|https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top17|https://m.media-amazon.com/images/M/MV5BY2NkZjEzMDgtN2RjYy00YzM1LWI4ZmQtMjIwYjFjNmI3ZGEwXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,3,380,562_.jpg|The+story+of+Henry+Hill+and+his+life+in+the+mob,+covering+his+relationship+with+his+wife+Karen+Hill+and+his+mob+partners+Jimmy+Conway+and+Tommy+DeVito+in+the+Italian-American+crime+syndicate.|https://www.youtube.com/embed/2ilzidi_J8Q|Martin+Scorsese|Nicholas+Pileggi+(book+"Wiseguy")|Martin+Scorsese+(screenplay)|tt0099685|One+Flew+Over+the+Cuckoo's+Nest|https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UY67_CR0,0,45,67_AL_.jpg|8.6|top18|https://m.media-amazon.com/images/M/MV5BZjA0OWVhOTAtYWQxNi00YzNhLWI4ZjYtNjFjZTEyYjJlNDVlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UX380_CR0,1,380,562_.jpg|In+the+Fall+of+1963,+a+Korean+War+veteran+and+criminal+pleads+insanity+and+is+admitted+to+a+mental+institution,+where+he+rallies+up+the+scared+patients+against+the+tyrannical+nurse.|https://www.youtube.com/embed/OXrcDonY-B8|Milos+Forman|Lawrence+Hauben+(screenplay)|Bo+Goldman+(screenplay)|Ken+Kesey+(based+on+the+novel+by)|tt0073486|Se7en|https://m.media-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|top19|https://m.media-amazon.com/images/M/MV5BOTUwODM5MTctZjczMi00OTk4LTg3NWUtNmVhMTAzNTNjYjcyXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,16,380,562_.jpg|Two+detectives,+a+rookie+and+a+veteran,+hunt+a+serial+killer+who+uses+the+seven+deadly+sins+as+his+motives.|https://www.youtube.com/embed/znmZoVkCjpI|Mystery|David+Fincher|Andrew+Kevin+Walker|tt0114369|Seven+Samurai|https://m.media-amazon.com/images/M/MV5BNWQ3OTM4ZGItMWEwZi00MjI5LWI3YzgtNTYwNWRkNmIzMGI5XkEyXkFqcGdeQXVyNDY2MTk1ODk@._V1_UY67_CR1,0,45,67_AL_.jpg|top20|https://m.media-amazon.com/images/M/MV5BNWQ3OTM4ZGItMWEwZi00MjI5LWI3YzgtNTYwNWRkNmIzMGI5XkEyXkFqcGdeQXVyNDY2MTk1ODk@._V1_QL75_UY562_CR11,0,380,562_.jpg|Farmers+from+a+village+exploited+by+bandits+hire+a+veteran+samurai+for+protection,+who+gathers+six+other+samurai+to+join+him.+As+the+samurai+teach+the+peasants+how+to+defend+themselves,+the+village+is+attacked+by+an+army+of+bandits.|https://www.youtube.com/embed/wJ1TOratCTo|Akira+Kurosawa|Akira+Kurosawa+(screenplay+by)|Shinobu+Hashimoto+(screenplay+by)|Hideo+Oguni+(screenplay+by)|tt0047478|It's+a+Wonderful+Life|https://m.media-amazon.com/images/M/MV5BZjc4NDZhZWMtNGEzYS00ZWU2LThlM2ItNTA0YzQ0OTExMTE2XkEyXkFqcGdeQXVyNjUwMzI2NzU@._V1_UY67_CR0,0,45,67_AL_.jpg|top21|https://m.media-amazon.com/images/M/MV5BZjc4NDZhZWMtNGEzYS00ZWU2LThlM2ItNTA0YzQ0OTExMTE2XkEyXkFqcGdeQXVyNjUwMzI2NzU@._V1_QL75_UY562_CR3,0,380,562_.jpg|An+angel+is+sent+from+Heaven+to+help+a+desperately+frustrated+businessman+by+showing+him+what+life+would+have+been+like+if+he+had+never+existed.|https://www.youtube.com/embed/iLR3gZrU2Xo|Family|Frank+Capra|Frances+Goodrich+(screenplay+by)|Albert+Hackett+(screenplay+by)|Frank+Capra+(screenplay+by)|tt0038650|The+Silence+of+the+Lambs|https://m.media-amazon.com/images/M/MV5BNjNhZTk0ZmEtNjJhMi00YzFlLWE1MmEtYzM1M2ZmMGMwMTU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR0,0,45,67_AL_.jpg|top22|https://m.media-amazon.com/images/M/MV5BNjNhZTk0ZmEtNjJhMi00YzFlLWE1MmEtYzM1M2ZmMGMwMTU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UY562_CR1,0,380,562_.jpg|A+young+F.B.I.+cadet+must+receive+the+help+of+an+incarcerated+and+manipulative+cannibal+killer+to+help+catch+another+serial+killer,+a+madman+who+skins+his+victims.|https://www.youtube.com/embed/W6Mm8Sbe__o|Thriller|Jonathan+Demme|Thomas+Harris+(based+on+the+novel+by)|Ted+Tally+(screenplay+by)|tt0102926|City+of+God|https://m.media-amazon.com/images/M/MV5BOTMwYjc5ZmItYTFjZC00ZGQ3LTlkNTMtMjZiNTZlMWQzNzI5XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top23|https://m.media-amazon.com/images/M/MV5BOTMwYjc5ZmItYTFjZC00ZGQ3LTlkNTMtMjZiNTZlMWQzNzI5XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,2,380,562_.jpg|In+the+slums+of+Rio,+two+kids'+paths+diverge+as+one+struggles+to+become+a+photographer+and+the+other+a+kingpin.|https://www.youtube.com/embed/dcUOO4Itgmw|Fernando+Meirelles|Kátia+Lund+(co-director)|Paulo+Lins+(novel)|Bráulio+Mantovani+(screenplay)|tt0317248|Saving+Private+Ryan|https://m.media-amazon.com/images/M/MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UY67_CR0,0,45,67_AL_.jpg|top24|https://m.media-amazon.com/images/M/MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_QL75_UY562_CR1,0,380,562_.jpg|Following+the+Normandy+Landings,+a+group+of+U.S.+soldiers+go+behind+enemy+lines+to+retrieve+a+paratrooper+whose+brothers+have+been+killed+in+action.|https://www.youtube.com/embed/9CiW_DgxCnQ|War|Steven+Spielberg|Robert+Rodat|tt0120815|Life+Is+Beautiful|https://m.media-amazon.com/images/M/MV5BYmJmM2Q4NmMtYThmNC00ZjRlLWEyZmItZTIwOTBlZDQ3NTQ1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX45_CR0,0,45,67_AL_.jpg|top25|https://m.media-amazon.com/images/M/MV5BYmJmM2Q4NmMtYThmNC00ZjRlLWEyZmItZTIwOTBlZDQ3NTQ1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UX380_CR0,2,380,562_.jpg|When+an+open-minded+Jewish+waiter+and+his+son+become+victims+of+the+Holocaust,+he+uses+a+perfect+mixture+of+will,+humor,+and+imagination+to+protect+his+son+from+the+dangers+around+their+camp.|https://www.youtube.com/embed/pAYEQP8gx3w|Comedy|Roberto+Benigni|Vincenzo+Cerami+(story+by)|Roberto+Benigni+(story+by)|tt0118799|Interstellar|https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UY67_CR0,0,45,67_AL_.jpg|top26|https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+team+of+explorers+travel+through+a+wormhole+in+space+in+an+attempt+to+ensure+humanity's+survival.|https://www.youtube.com/embed/4T4wxDnTYLg|Christopher+Nolan|Jonathan+Nolan|Christopher+Nolan|tt0816692|The+Green+Mile|https://m.media-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1_UY67_CR0,0,45,67_AL_.jpg|top27|https://m.media-amazon.com/images/M/MV5BMTUxMzQyNjA5MF5BMl5BanBnXkFtZTYwOTU2NTY3._V1_QL75_UX380_CR0,0,380,562_.jpg|The+lives+of+guards+on+Death+Row+are+affected+by+one+of+their+charges:+a+black+man+accused+of+child+murder+and+rape,+yet+who+has+a+mysterious+gift.|https://www.youtube.com/embed/Ki4haFrqSrw|Frank+Darabont|Stephen+King+(novel)|Frank+Darabont+(screenplay)|tt0120689|Star+Wars:+Episode+IV+-+A+New+Hope|https://m.media-amazon.com/images/M/MV5BOTA5NjhiOTAtZWM0ZC00MWNhLThiMzEtZDFkOTk2OTU1ZDJkXkEyXkFqcGdeQXVyMTA4NDI1NTQx._V1_UX45_CR0,0,45,67_AL_.jpg|top28|https://m.media-amazon.com/images/M/MV5BOTA5NjhiOTAtZWM0ZC00MWNhLThiMzEtZDFkOTk2OTU1ZDJkXkEyXkFqcGdeQXVyMTA4NDI1NTQx._V1_QL75_UX380_CR0,9,380,562_.jpg|Luke+Skywalker+joins+forces+with+a+Jedi+Knight,+a+cocky+pilot,+a+Wookiee+and+two+droids+to+save+the+galaxy+from+the+Empire's+world-destroying+battle+station,+while+also+attempting+to+rescue+Princess+Leia+from+the+mysterious+Darth+Vader.|https://www.youtube.com/embed/L-_xHEv0l-w|George+Lucas|George+Lucas|tt0076759|Terminator+2:+Judgment+Day|https://m.media-amazon.com/images/M/MV5BMGU2NzRmZjUtOGUxYS00ZjdjLWEwZWItY2NlM2JhNjkxNTFmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR0,0,45,67_AL_.jpg|8.5|top29|https://m.media-amazon.com/images/M/MV5BMGU2NzRmZjUtOGUxYS00ZjdjLWEwZWItY2NlM2JhNjkxNTFmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,1,380,562_.jpg|A+cyborg,+identical+to+the+one+who+failed+to+kill+Sarah+Connor,+must+now+protect+her+ten-year-old+son+John+from+a+more+advanced+and+powerful+cyborg.|https://www.youtube.com/embed/CRRlbK5w8AE|James+Cameron|James+Cameron|William+Wisher|tt0103064|Back+to+the+Future|https://m.media-amazon.com/images/M/MV5BZmU0M2Y1OGUtZjIxNi00ZjBkLTg1MjgtOWIyNThiZWIwYjRiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX45_CR0,0,45,67_AL_.jpg|top30|https://m.media-amazon.com/images/M/MV5BZmU0M2Y1OGUtZjIxNi00ZjBkLTg1MjgtOWIyNThiZWIwYjRiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UX380_CR0,14,380,562_.jpg|Marty+McFly,+a+17-year-old+high+school+student,+is+accidentally+sent+30+years+into+the+past+in+a+time-traveling+DeLorean+invented+by+his+close+friend,+the+maverick+scientist+Doc+Brown.|https://www.youtube.com/embed/qvsgGtivCgs|Robert+Zemeckis|Robert+Zemeckis|Bob+Gale|tt0088763|Spirited+Away|https://m.media-amazon.com/images/M/MV5BMjlmZmI5MDctNDE2YS00YWE0LWE5ZWItZDBhYWQ0NTcxNWRhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UY67_CR0,0,45,67_AL_.jpg|top31|https://m.media-amazon.com/images/M/MV5BMjlmZmI5MDctNDE2YS00YWE0LWE5ZWItZDBhYWQ0NTcxNWRhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_QL75_UX380_CR0,0,380,562_.jpg|During+her+family's+move+to+the+suburbs,+a+sullen+10-year-old+girl+wanders+into+a+world+ruled+by+gods,+witches,+and+spirits,+and+where+humans+are+changed+into+beasts.|https://www.youtube.com/embed/ByXuk9QqQkk|Animation|Hayao+Miyazaki|Hayao+Miyazaki|tt0245429|Psycho|https://m.media-amazon.com/images/M/MV5BNTQwNDM1YzItNDAxZC00NWY2LTk0M2UtNDIwNWI5OGUyNWUxXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR0,0,45,67_AL_.jpg|top32|https://m.media-amazon.com/images/M/MV5BNTQwNDM1YzItNDAxZC00NWY2LTk0M2UtNDIwNWI5OGUyNWUxXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,1,380,562_.jpg|A+Phoenix+secretary+embezzles+$40,000+from+her+employer's+client,+goes+on+the+run+and+checks+into+a+remote+motel+run+by+a+young+man+under+the+domination+of+his+mother.|https://www.youtube.com/embed/DTJQfFQ40lI|Horror|Alfred+Hitchcock|Joseph+Stefano+(screenplay+by)|Robert+Bloch+(based+on+the+novel+by)|tt0054215|The+Pianist|https://m.media-amazon.com/images/M/MV5BOWRiZDIxZjktMTA1NC00MDQ2LWEzMjUtMTliZmY3NjQ3ODJiXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR2,0,45,67_AL_.jpg|top33|https://m.media-amazon.com/images/M/MV5BOWRiZDIxZjktMTA1NC00MDQ2LWEzMjUtMTliZmY3NjQ3ODJiXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UY562_CR14,0,380,562_.jpg|A+Polish+Jewish+musician+struggles+to+survive+the+destruction+of+the+Warsaw+ghetto+of+World+War+II.|https://www.youtube.com/embed/BFwGqLa_oAo|Music|Roman+Polanski|Ronald+Harwood+(screenplay+by)|Wladyslaw+Szpilman+(based+on+the+book+by)|tt0253474|Parasite|https://m.media-amazon.com/images/M/MV5BYWZjMjk3ZTItODQ2ZC00NTY5LWE0ZDYtZTI3MjcwN2Q5NTVkXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_UY67_CR0,0,45,67_AL_.jpg|top34|https://m.media-amazon.com/images/M/MV5BYWZjMjk3ZTItODQ2ZC00NTY5LWE0ZDYtZTI3MjcwN2Q5NTVkXkEyXkFqcGdeQXVyODk4OTc3MTY@._V1_QL75_UX380_CR0,0,380,562_.jpg|Greed+and+class+discrimination+threaten+the+newly+formed+symbiotic+relationship+between+the+wealthy+Park+family+and+the+destitute+Kim+clan.|https://www.youtube.com/embed/5xH0HfJHsaY|Bong+Joon+Ho|Bong+Joon+Ho+(story+by)|Han+Jin-won+(screenplay+by)|tt6751668|Léon:+The+Professional|https://m.media-amazon.com/images/M/MV5BOTgyMWQ0ZWUtN2Q2MS00NmY0LWI3OWMtNjFkMzZlNDZjNTk0XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UX45_CR0,0,45,67_AL_.jpg|top35|https://m.media-amazon.com/images/M/MV5BOTgyMWQ0ZWUtN2Q2MS00NmY0LWI3OWMtNjFkMzZlNDZjNTk0XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,2,380,562_.jpg|12-year-old+Mathilda+is+reluctantly+taken+in+by+Léon,+a+professional+assassin,+after+her+family+is+murdered.+An+unusual+relationship+forms+as+she+becomes+his+protégée+and+learns+the+assassin's+trade.|https://www.youtube.com/embed/jawVxq1Iyl0|Luc+Besson|Luc+Besson|tt0110413|The+Lion+King|https://m.media-amazon.com/images/M/MV5BYTYxNGMyZTYtMjE3MS00MzNjLWFjNmYtMDk3N2FmM2JiM2M1XkEyXkFqcGdeQXVyNjY5NDU4NzI@._V1_UY67_CR0,0,45,67_AL_.jpg|top36|https://m.media-amazon.com/images/M/MV5BYTYxNGMyZTYtMjE3MS00MzNjLWFjNmYtMDk3N2FmM2JiM2M1XkEyXkFqcGdeQXVyNjY5NDU4NzI@._V1_QL75_UX380_CR0,1,380,562_.jpg|Lion+prince+Simba+and+his+father+are+targeted+by+his+bitter+uncle,+who+wants+to+ascend+the+throne+himself.|https://www.youtube.com/embed/eHcZlPpNt0Q|Roger+Allers|Rob+Minkoff|Irene+Mecchi+(screenplay+by)|Jonathan+Roberts+(screenplay+by)|Linda+Woolverton+(screenplay+by)|tt0110357|Gladiator|https://m.media-amazon.com/images/M/MV5BMDliMmNhNDEtODUyOS00MjNlLTgxODEtN2U3NzIxMGVkZTA1L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR0,0,45,67_AL_.jpg|top37|https://m.media-amazon.com/images/M/MV5BMDliMmNhNDEtODUyOS00MjNlLTgxODEtN2U3NzIxMGVkZTA1L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+former+Roman+General+sets+out+to+exact+vengeance+against+the+corrupt+emperor+who+murdered+his+family+and+sent+him+into+slavery.|https://www.youtube.com/embed/uvbavW31adA|Ridley+Scott|David+Franzoni+(story)|John+Logan+(screenplay)|William+Nicholson+(screenplay)|tt0172495|American+History+X|https://m.media-amazon.com/images/M/MV5BZTJhN2FkYWEtMGI0My00YWM4LWI2MjAtM2UwNjY4MTI2ZTQyXkEyXkFqcGdeQXVyNjc3MjQzNTI@._V1_UY67_CR0,0,45,67_AL_.jpg|top38|https://m.media-amazon.com/images/M/MV5BZTJhN2FkYWEtMGI0My00YWM4LWI2MjAtM2UwNjY4MTI2ZTQyXkEyXkFqcGdeQXVyNjc3MjQzNTI@._V1_QL75_UX380_CR0,1,380,562_.jpg|A+former+neo-nazi+skinhead+tries+to+prevent+his+younger+brother+from+going+down+the+same+wrong+path+that+he+did.|https://www.youtube.com/embed/XfQYHqsiN5g|Tony+Kaye|David+McKenna|tt0120586|The+Departed|https://m.media-amazon.com/images/M/MV5BMTI1MTY2OTIxNV5BMl5BanBnXkFtZTYwNjQ4NjY3._V1_UY67_CR0,0,45,67_AL_.jpg|top39|https://m.media-amazon.com/images/M/MV5BMTI1MTY2OTIxNV5BMl5BanBnXkFtZTYwNjQ4NjY3._V1_QL75_UY562_CR0,0,380,562_.jpg|An+undercover+cop+and+a+mole+in+the+police+attempt+to+identify+each+other+while+infiltrating+an+Irish+gang+in+South+Boston.|https://www.youtube.com/embed/iojhqm0JTW4|Martin+Scorsese|William+Monahan+(screenplay)|Alan+Mak+(2002+screenplay+Mou+gaan+dou)|Felix+Chong+(2002+screenplay+Mou+gaan+dou)|tt0407887|The+Usual+Suspects|https://m.media-amazon.com/images/M/MV5BYTViNjMyNmUtNDFkNC00ZDRlLThmMDUtZDU2YWE4NGI2ZjVmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|top40|https://m.media-amazon.com/images/M/MV5BYTViNjMyNmUtNDFkNC00ZDRlLThmMDUtZDU2YWE4NGI2ZjVmXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,2,380,562_.jpg|A+sole+survivor+tells+of+the+twisty+events+leading+up+to+a+horrific+gun+battle+on+a+boat,+which+began+when+five+criminals+met+at+a+seemingly+random+police+lineup.|https://www.youtube.com/embed/Q0eCiCinc4E|Bryan+Singer|Christopher+McQuarrie|tt0114814|The+Prestige|https://m.media-amazon.com/images/M/MV5BMjA4NDI0MTIxNF5BMl5BanBnXkFtZTYwNTM0MzY2._V1_UY67_CR0,0,45,67_AL_.jpg|top41|https://m.media-amazon.com/images/M/MV5BMjA4NDI0MTIxNF5BMl5BanBnXkFtZTYwNTM0MzY2._V1_QL75_UX380_CR0,0,380,562_.jpg|After+a+tragic+accident,+two+stage+magicians+in+1890s+London+engage+in+a+battle+to+create+the+ultimate+illusion+while+sacrificing+everything+they+have+to+outwit+each+other.|https://www.youtube.com/embed/RLtaA9fFNXU|Christopher+Nolan|Jonathan+Nolan+(screenplay)|Christopher+Nolan+(screenplay)|Christopher+Priest+(novel)|tt0482571|Whiplash|https://m.media-amazon.com/images/M/MV5BOTA5NDZlZGUtMjAxOS00YTRkLTkwYmMtYWQ0NWEwZDZiNjEzXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UY67_CR0,0,45,67_AL_.jpg|top42|https://m.media-amazon.com/images/M/MV5BOTA5NDZlZGUtMjAxOS00YTRkLTkwYmMtYWQ0NWEwZDZiNjEzXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+promising+young+drummer+enrolls+at+a+cut-throat+music+conservatory+where+his+dreams+of+greatness+are+mentored+by+an+instructor+who+will+stop+at+nothing+to+realize+a+student's+potential.|https://www.youtube.com/embed/7d_jQycdQGo|Damien+Chazelle|Damien+Chazelle|tt2582802|Casablanca|https://m.media-amazon.com/images/M/MV5BY2IzZGY2YmEtYzljNS00NTM5LTgwMzUtMzM1NjQ4NGI0OTk0XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UX45_CR0,0,45,67_AL_.jpg|top43|https://m.media-amazon.com/images/M/MV5BY2IzZGY2YmEtYzljNS00NTM5LTgwMzUtMzM1NjQ4NGI0OTk0XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_QL75_UX380_CR0,5,380,562_.jpg|A+cynical+expatriate+American+cafe+owner+struggles+to+decide+whether+or+not+to+help+his+former+lover+and+her+fugitive+husband+escape+the+Nazis+in+French+Morocco.|https://www.youtube.com/embed/BkL9l7qovsE|Michael+Curtiz|Julius+J.+Epstein+(screenplay)|Philip+G.+Epstein+(screenplay)|Howard+Koch+(screenplay)|tt0034583|The+Intouchables|https://m.media-amazon.com/images/M/MV5BMTYxNDA3MDQwNl5BMl5BanBnXkFtZTcwNTU4Mzc1Nw@@._V1_UY67_CR0,0,45,67_AL_.jpg|top44|https://m.media-amazon.com/images/M/MV5BMTYxNDA3MDQwNl5BMl5BanBnXkFtZTcwNTU4Mzc1Nw@@._V1_QL75_UX380_CR0,0,380,562_.jpg|After+he+becomes+a+quadriplegic+from+a+paragliding+accident,+an+aristocrat+hires+a+young+man+from+the+projects+to+be+his+caregiver.|https://www.youtube.com/embed/oK5hMNxqsFA|Olivier+Nakache|Éric+Toledano|Olivier+Nakache|Philippe+Pozzo+di+Borgo+(adapted+from+his+autobiographical+tale+Le+Second+Souffle)|Éric+Toledano|tt1675434|Harakiri|https://m.media-amazon.com/images/M/MV5BYjBmYTQ1NjItZWU5MS00YjI0LTg2OTYtYmFkN2JkMmNiNWVkXkEyXkFqcGdeQXVyMTMxMTY0OTQ@._V1_UY67_CR2,0,45,67_AL_.jpg|top45|https://m.media-amazon.com/images/M/MV5BYjBmYTQ1NjItZWU5MS00YjI0LTg2OTYtYmFkN2JkMmNiNWVkXkEyXkFqcGdeQXVyMTMxMTY0OTQ@._V1_QL75_UY562_CR17,0,380,562_.jpg|When+a+ronin+requesting+seppuku+at+a+feudal+lord's+palace+is+told+of+the+brutal+suicide+of+another+ronin+who+previously+visited,+he+reveals+how+their+pasts+are+intertwined+-+and+in+doing+so+challenges+the+clan's+integrity.|https://www.youtube.com/embed/gfABwM-Ppng|Masaki+Kobayashi|Yasuhiko+Takiguchi+(novel+"Ibun+rônin+ki")|Shinobu+Hashimoto+(screenplay)|tt0056058|Grave+of+the+Fireflies|https://m.media-amazon.com/images/M/MV5BZmY2NjUzNDQtNTgxNC00M2Q4LTljOWQtMjNjNDBjNWUxNmJlXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_UX45_CR0,0,45,67_AL_.jpg|top46|https://m.media-amazon.com/images/M/MV5BZmY2NjUzNDQtNTgxNC00M2Q4LTljOWQtMjNjNDBjNWUxNmJlXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_QL75_UX380_CR0,4,380,562_.jpg|A+young+boy+and+his+little+sister+struggle+to+survive+in+Japan+during+World+War+II.|https://www.youtube.com/embed/4vPeTSRd580|Isao+Takahata|Akiyuki+Nosaka+(novel)|Isao+Takahata|tt0095327|Modern+Times|https://m.media-amazon.com/images/M/MV5BYjJiZjMzYzktNjU0NS00OTkxLWEwYzItYzdhYWJjN2QzMTRlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|8.4|top47|https://m.media-amazon.com/images/M/MV5BYjJiZjMzYzktNjU0NS00OTkxLWEwYzItYzdhYWJjN2QzMTRlL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,15,380,562_.jpg|The+Tramp+struggles+to+live+in+modern+industrial+society+with+the+help+of+a+young+homeless+woman.|https://www.youtube.com/embed/GLeDdzGUTq0|Charles+Chaplin|Charles+Chaplin|tt0027977|Once+Upon+a+Time+in+the+West|https://m.media-amazon.com/images/M/MV5BODQ3NDExOGYtMzI3Mi00NWRlLTkwNjAtNjc4MDgzZGJiZTA1XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UX45_CR0,0,45,67_AL_.jpg|top48|https://m.media-amazon.com/images/M/MV5BODQ3NDExOGYtMzI3Mi00NWRlLTkwNjAtNjc4MDgzZGJiZTA1XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,3,380,562_.jpg|A+mysterious+stranger+with+a+harmonica+joins+forces+with+a+notorious+desperado+to+protect+a+beautiful+widow+from+a+ruthless+assassin+working+for+the+railroad.|https://www.youtube.com/embed/c8CJ6L0I6W8|Sergio+Leone|Sergio+Donati+(screenplay+by)|Sergio+Leone+(screenplay+by)|Dario+Argento+(from+a+story+by)|tt0064116|Rear+Window|https://m.media-amazon.com/images/M/MV5BNGUxYWM3M2MtMGM3Mi00ZmRiLWE0NGQtZjE5ODI2OTJhNTU0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UY67_CR0,0,45,67_AL_.jpg|top49|https://m.media-amazon.com/images/M/MV5BNGUxYWM3M2MtMGM3Mi00ZmRiLWE0NGQtZjE5ODI2OTJhNTU0XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UY562_CR2,0,380,562_.jpg|A+wheelchair-bound+photographer+spies+on+his+neighbors+from+his+Greenwich+Village+courtyard+apartment+window,+and+becomes+convinced+one+of+them+has+committed+murder,+despite+the+skepticism+of+his+fashion-model+girlfriend.|https://www.youtube.com/embed/m01YktiEZCw|Alfred+Hitchcock|John+Michael+Hayes+(screenplay)|Cornell+Woolrich+(short+story)|tt0047396|Alien|https://m.media-amazon.com/images/M/MV5BOGQzZTBjMjQtOTVmMS00NGE5LWEyYmMtOGQ1ZGZjNmRkYjFhXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UX45_CR0,0,45,67_AL_.jpg|top50|https://m.media-amazon.com/images/M/MV5BOGQzZTBjMjQtOTVmMS00NGE5LWEyYmMtOGQ1ZGZjNmRkYjFhXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,6,380,562_.jpg|The+crew+of+a+commercial+spacecraft+encounter+a+deadly+lifeform+after+investigating+an+unknown+transmission.|https://www.youtube.com/embed/jQ5lPt9edzQ|Ridley+Scott|Dan+O'Bannon+(screenplay+by)|Ronald+Shusett+(story+by)|tt0078748|City+Lights|https://m.media-amazon.com/images/M/MV5BY2I4MmM1N2EtM2YzOS00OWUzLTkzYzctNDc5NDg2N2IyODJmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top51|https://m.media-amazon.com/images/M/MV5BY2I4MmM1N2EtM2YzOS00OWUzLTkzYzctNDc5NDg2N2IyODJmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,4,380,562_.jpg|With+the+aid+of+a+wealthy+erratic+tippler,+a+dewy-eyed+tramp+who+has+fallen+in+love+with+a+sightless+flower+girl+accumulates+money+to+be+able+to+help+her+medically.|https://www.youtube.com/embed/7vl7F8S4cpQ|Charles+Chaplin|Charles+Chaplin|Harry+Carr+(uncredited)|Harry+Crocker+(uncredited)|tt0021749|Cinema+Paradiso|https://m.media-amazon.com/images/M/MV5BM2FhYjEyYmYtMDI1Yy00YTdlLWI2NWQtYmEzNzAxOGY1NjY2XkEyXkFqcGdeQXVyNTA3NTIyNDg@._V1_UY67_CR0,0,45,67_AL_.jpg|top52|https://m.media-amazon.com/images/M/MV5BM2FhYjEyYmYtMDI1Yy00YTdlLWI2NWQtYmEzNzAxOGY1NjY2XkEyXkFqcGdeQXVyNTA3NTIyNDg@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+filmmaker+recalls+his+childhood+when+falling+in+love+with+the+pictures+at+the+cinema+of+his+home+village+and+forms+a+deep+friendship+with+the+cinema's+projectionist.|https://www.youtube.com/embed/C2-GX0Tltgw|Giuseppe+Tornatore|Giuseppe+Tornatore+(story)|Vanna+Paoli+(collaborating+writer)|tt0095765|Apocalypse+Now|https://m.media-amazon.com/images/M/MV5BYmQyNTA1ZGItNjZjMi00NzFlLWEzMWEtNWMwN2Q2MjJhYzEyXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UX45_CR0,0,45,67_AL_.jpg|top53|https://m.media-amazon.com/images/M/MV5BYmQyNTA1ZGItNjZjMi00NzFlLWEzMWEtNWMwN2Q2MjJhYzEyXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,11,380,562_.jpg|A+U.S.+Army+officer+serving+in+Vietnam+is+tasked+with+assassinating+a+renegade+Special+Forces+Colonel+who+sees+himself+as+a+god.|https://www.youtube.com/embed/FTjG-Aux_yQ|Francis+Ford+Coppola|John+Milius|Francis+Ford+Coppola|Michael+Herr+(narration)|tt0078788|Memento|https://m.media-amazon.com/images/M/MV5BZTcyNjk1MjgtOWI3Mi00YzQwLWI5MTktMzY4ZmI2NDAyNzYzXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR0,0,45,67_AL_.jpg|top54|https://m.media-amazon.com/images/M/MV5BZTcyNjk1MjgtOWI3Mi00YzQwLWI5MTktMzY4ZmI2NDAyNzYzXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UY562_CR2,0,380,562_.jpg|A+man+with+short-term+memory+loss+attempts+to+track+down+his+wife's+murderer.|https://www.youtube.com/embed/4CV41hoyS8A|Christopher+Nolan|Christopher+Nolan+(screenplay)|Jonathan+Nolan+(short+story+"Memento+Mori")|tt0209144|Indiana+Jones+and+the+Raiders+of+the+Lost+Ark|https://m.media-amazon.com/images/M/MV5BNTU2ODkyY2MtMjU1NC00NjE1LWEzYjgtMWQ3MzRhMTE0NDc0XkEyXkFqcGdeQXVyMjM4MzQ4OTQ@._V1_UY67_CR0,0,45,67_AL_.jpg|top55|https://m.media-amazon.com/images/M/MV5BNTU2ODkyY2MtMjU1NC00NjE1LWEzYjgtMWQ3MzRhMTE0NDc0XkEyXkFqcGdeQXVyMjM4MzQ4OTQ@._V1_QL75_UY562_CR1,0,380,562_.jpg|Archaeology+professor+Indiana+Jones+ventures+to+seize+a+biblical+artefact+known+as+the+Ark+of+the+Covenant.+While+doing+so,+he+puts+up+a+fight+against+Renee+and+a+troop+of+Nazis.|https://www.youtube.com/embed/Rh_BJXG1-44|Steven+Spielberg|Lawrence+Kasdan+(screenplay+by)|George+Lucas+(story+by)|Philip+Kaufman+(story+by)|tt0082971|Django+Unchained|https://m.media-amazon.com/images/M/MV5BMjIyNTQ5NjQ1OV5BMl5BanBnXkFtZTcwODg1MDU4OA@@._V1_UY67_CR0,0,45,67_AL_.jpg|top56|https://m.media-amazon.com/images/M/MV5BMjIyNTQ5NjQ1OV5BMl5BanBnXkFtZTcwODg1MDU4OA@@._V1_QL75_UX380_CR0,0,380,562_.jpg|With+the+help+of+a+German+bounty-hunter,+a+freed+slave+sets+out+to+rescue+his+wife+from+a+brutal+plantation-owner+in+Mississippi.|https://www.youtube.com/embed/0fUCuvNlOCg|Quentin+Tarantino|Quentin+Tarantino|tt1853728|WALL·E|https://m.media-amazon.com/images/M/MV5BMjExMTg5OTU0NF5BMl5BanBnXkFtZTcwMjMxMzMzMw@@._V1_UY67_CR0,0,45,67_AL_.jpg|top57|https://m.media-amazon.com/images/M/MV5BMjExMTg5OTU0NF5BMl5BanBnXkFtZTcwMjMxMzMzMw@@._V1_QL75_UX380_CR0,0,380,562_.jpg|In+the+distant+future,+a+small+waste-collecting+robot+inadvertently+embarks+on+a+space+journey+that+will+ultimately+decide+the+fate+of+mankind.|https://www.youtube.com/embed/CZ1CATNbXg0|Andrew+Stanton|Andrew+Stanton+(original+story+by)|Pete+Docter+(original+story+by)|Jim+Reardon+(screenplay+by)|tt0910970|The+Lives+of+Others|https://m.media-amazon.com/images/M/MV5BNmQyNmJjM2ItNTQzYi00ZjMxLWFjMDYtZjUyN2YwZDk5YWQ2XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_UX45_CR0,0,45,67_AL_.jpg|top58|https://m.media-amazon.com/images/M/MV5BNmQyNmJjM2ItNTQzYi00ZjMxLWFjMDYtZjUyN2YwZDk5YWQ2XkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_QL75_UX380_CR0,3,380,562_.jpg|In+1984+East+Berlin,+an+agent+of+the+secret+police,+conducting+surveillance+on+a+writer+and+his+lover,+finds+himself+becoming+increasingly+absorbed+by+their+lives.|https://www.youtube.com/embed/YsShZNHmpGE|Florian+Henckel+von+Donnersmarck|Florian+Henckel+von+Donnersmarck|tt0405094|Sunset+Blvd.|https://m.media-amazon.com/images/M/MV5BMTU0NTkyNzYwMF5BMl5BanBnXkFtZTgwMDU0NDk5MTI@._V1_UX45_CR0,0,45,67_AL_.jpg|top59|https://m.media-amazon.com/images/M/MV5BMTU0NTkyNzYwMF5BMl5BanBnXkFtZTgwMDU0NDk5MTI@._V1_QL75_UX380_CR0,5,380,562_.jpg|A+screenwriter+develops+a+dangerous+relationship+with+a+faded+film+star+determined+to+make+a+triumphant+return.|https://www.youtube.com/embed/_dY0SVxnHjQ|Film-Noir|Billy+Wilder|Charles+Brackett|Billy+Wilder|D.M.+Marshman+Jr.|tt0043014|Paths+of+Glory|https://m.media-amazon.com/images/M/MV5BOTI5Nzc0OTMtYzBkMS00NjkxLThmM2UtNjM2ODgxN2M5NjNkXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top60|https://m.media-amazon.com/images/M/MV5BOTI5Nzc0OTMtYzBkMS00NjkxLThmM2UtNjM2ODgxN2M5NjNkXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_QL75_UX380_CR0,10,380,562_.jpg|After+refusing+to+attack+an+enemy+position,+a+general+accuses+the+soldiers+of+cowardice+and+their+commanding+officer+must+defend+them.|https://www.youtube.com/embed/AV9XLjDbt5A|Stanley+Kubrick|Stanley+Kubrick+(screenplay)|Calder+Willingham+(screenplay)|Jim+Thompson+(screenplay)|tt0050825|The+Shining|https://m.media-amazon.com/images/M/MV5BZWFlYmY2MGEtZjVkYS00YzU4LTg0YjQtYzY1ZGE3NTA5NGQxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX45_CR0,0,45,67_AL_.jpg|top61|https://m.media-amazon.com/images/M/MV5BZWFlYmY2MGEtZjVkYS00YzU4LTg0YjQtYzY1ZGE3NTA5NGQxXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UX380_CR0,8,380,562_.jpg|A+family+heads+to+an+isolated+hotel+for+the+winter+where+a+sinister+presence+influences+the+father+into+violence,+while+his+psychic+son+sees+horrific+forebodings+from+both+past+and+future.|https://www.youtube.com/embed/FZQvIJxG9Xs|Stanley+Kubrick|Stephen+King+(based+upon+the+novel+by)|Stanley+Kubrick+(screenplay+by)|Diane+Johnson+(screenplay+by)|tt0081505|The+Great+Dictator|https://m.media-amazon.com/images/M/MV5BMmExYWJjNTktNGUyZS00ODhmLTkxYzAtNWIzOGEyMGNiMmUwXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR0,0,45,67_AL_.jpg|top62|https://m.media-amazon.com/images/M/MV5BMmExYWJjNTktNGUyZS00ODhmLTkxYzAtNWIzOGEyMGNiMmUwXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UY562_CR0,0,380,562_.jpg|Dictator+Adenoid+Hynkel+tries+to+expand+his+empire+while+a+poor+Jewish+barber+tries+to+avoid+persecution+from+Hynkel's+regime.|https://www.youtube.com/embed/k8bVG8XC-4I|Charles+Chaplin|Charles+Chaplin|tt0032553|Avengers:+Infinity+War|https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_UY67_CR0,0,45,67_AL_.jpg|top63|https://m.media-amazon.com/images/M/MV5BMjMxNjY2MDU1OV5BMl5BanBnXkFtZTgwNzY1MTUwNTM@._V1_QL75_UX380_CR0,0,380,562_.jpg|The+Avengers+and+their+allies+must+be+willing+to+sacrifice+all+in+an+attempt+to+defeat+the+powerful+Thanos+before+his+blitz+of+devastation+and+ruin+puts+an+end+to+the+universe.|https://www.youtube.com/embed/B65hW9YYY5A|Anthony+Russo|Joe+Russo|Christopher+Markus+(screenplay+by)|Stephen+McFeely+(screenplay+by)|Stan+Lee+(based+on+the+Marvel+comics+by)|tt4154756|Witness+for+the+Prosecution|https://m.media-amazon.com/images/M/MV5BNDQwODU5OWYtNDcyNi00MDQ1LThiOGMtZDkwNWJiM2Y3MDg0XkEyXkFqcGdeQXVyMDI2NDg0NQ@@._V1_UX45_CR0,0,45,67_AL_.jpg|top64|https://m.media-amazon.com/images/M/MV5BNDQwODU5OWYtNDcyNi00MDQ1LThiOGMtZDkwNWJiM2Y3MDg0XkEyXkFqcGdeQXVyMDI2NDg0NQ@@._V1_QL75_UX380_CR0,10,380,562_.jpg|A+veteran+British+barrister+must+defend+his+client+in+a+murder+trial+that+has+surprise+after+surprise.|https://www.youtube.com/embed/GMlJfiA2u7Y|Billy+Wilder|Agatha+Christie+(in+Agatha+Christie's+international+stage+success)|Billy+Wilder+(screen+play)|Harry+Kurnitz+(screen+play)|tt0051201|Aliens|https://m.media-amazon.com/images/M/MV5BZGU2OGY5ZTYtMWNhYy00NjZiLWI0NjUtZmNhY2JhNDRmODU3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|8.3|top65|https://m.media-amazon.com/images/M/MV5BZGU2OGY5ZTYtMWNhYy00NjZiLWI0NjUtZmNhY2JhNDRmODU3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,14,380,562_.jpg|Fifty-seven+years+after+surviving+an+apocalyptic+attack+aboard+her+space+vessel+by+merciless+space+creatures,+Officer+Ripley+awakens+from+hyper-sleep+and+tries+to+warn+anyone+who+will+listen+about+the+predators.|https://www.youtube.com/embed/oSeQQlaCZgU|James+Cameron|James+Cameron+(story+by)|David+Giler+(story+by)|Walter+Hill+(story+by)|tt0090605|American+Beauty|https://m.media-amazon.com/images/M/MV5BNTBmZWJkNjctNDhiNC00MGE2LWEwOTctZTk5OGVhMWMyNmVhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_UY67_CR0,0,45,67_AL_.jpg|top66|https://m.media-amazon.com/images/M/MV5BNTBmZWJkNjctNDhiNC00MGE2LWEwOTctZTk5OGVhMWMyNmVhXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+sexually+frustrated+suburban+father+has+a+mid-life+crisis+after+becoming+infatuated+with+his+daughter's+best+friend.|https://www.youtube.com/embed/Ly7rq5EsTC8|Sam+Mendes|Alan+Ball|tt0169547|Spider-Man:+Into+the+Spider-Verse|https://m.media-amazon.com/images/M/MV5BMjMwNDkxMTgzOF5BMl5BanBnXkFtZTgwNTkwNTQ3NjM@._V1_UY67_CR0,0,45,67_AL_.jpg|top67|https://m.media-amazon.com/images/M/MV5BMjMwNDkxMTgzOF5BMl5BanBnXkFtZTgwNTkwNTQ3NjM@._V1_QL75_UX380_CR0,1,380,562_.jpg|Teen+Miles+Morales+becomes+the+Spider-Man+of+his+universe,+and+must+join+with+five+spider-powered+individuals+from+other+dimensions+to+stop+a+threat+for+all+realities.|https://www.youtube.com/embed/g4Hbz2jLxvQ|Bob+Persichetti|Peter+Ramsey|Rodney+Rothman|Phil+Lord+(screenplay+by)|Rodney+Rothman+(screenplay+by)|tt4633694|Dr.+Strangelove+or:+How+I+Learned+to+Stop+Worrying+and+Love+the+Bomb|https://m.media-amazon.com/images/M/MV5BZWI3ZTMxNjctMjdlNS00NmUwLWFiM2YtZDUyY2I3N2MxYTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top68|https://m.media-amazon.com/images/M/MV5BZWI3ZTMxNjctMjdlNS00NmUwLWFiM2YtZDUyY2I3N2MxYTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,14,380,562_.jpg|An+insane+American+general+orders+a+bombing+attack+on+the+Soviet+Union,+triggering+a+path+to+nuclear+holocaust+that+a+war+room+full+of+politicians+and+generals+frantically+tries+to+stop.|https://www.youtube.com/embed/jPU1AYTxwg4|Stanley+Kubrick|Stanley+Kubrick+(screenplay)|Terry+Southern+(screenplay)|Peter+George+(screenplay)|tt0057012|The+Dark+Knight+Rises|https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_UY67_CR0,0,45,67_AL_.jpg|top69|https://m.media-amazon.com/images/M/MV5BMTk4ODQzNDY3Ml5BMl5BanBnXkFtZTcwODA0NTM4Nw@@._V1_QL75_UX380_CR0,0,380,562_.jpg|Eight+years+after+the+Joker's+reign+of+anarchy,+Batman,+with+the+help+of+the+enigmatic+Selina+Kyle,+is+forced+from+his+exile+to+save+Gotham+City+from+the+brutal+guerrilla+terrorist+Bane.|https://www.youtube.com/embed/GokKUqLcvD8|Christopher+Nolan|Jonathan+Nolan+(screenplay)|Christopher+Nolan+(screenplay)|David+S.+Goyer+(story)|tt1345836|Oldboy|https://m.media-amazon.com/images/M/MV5BMTI3NTQyMzU5M15BMl5BanBnXkFtZTcwMTM2MjgyMQ@@._V1_UY67_CR0,0,45,67_AL_.jpg|top70|https://m.media-amazon.com/images/M/MV5BMTI3NTQyMzU5M15BMl5BanBnXkFtZTcwMTM2MjgyMQ@@._V1_QL75_UX380_CR0,0,380,562_.jpg|After+being+kidnapped+and+imprisoned+for+fifteen+years,+Oh+Dae-Su+is+released,+only+to+find+that+he+must+find+his+captor+in+five+days.|https://www.youtube.com/embed/2HkjrJ6IK5E|Park+Chan-wook|Garon+Tsuchiya+(manga)|Nobuaki+Minegishi+(manga)|Park+Chan-wook+(character+created+by:+Oldboy|Vengeance+Trilogy)|tt0364569|Joker|https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UY67_CR0,0,45,67_AL_.jpg|top71|https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_QL75_UX380_CR0,0,380,562_.jpg|A+mentally+troubled+stand-up+comedian+embarks+on+a+downward+spiral+that+leads+to+the+creation+of+an+iconic+villain.|https://www.youtube.com/embed/zAGVQLHvwOY|Todd+Phillips|Todd+Phillips|Scott+Silver|Bob+Kane+(based+on+characters+created+by)|tt7286456|Amadeus|https://m.media-amazon.com/images/M/MV5BNWJlNzUzNGMtYTAwMS00ZjI2LWFmNWQtODcxNWUxODA5YmU1XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_UX45_CR0,0,45,67_AL_.jpg|top72|https://m.media-amazon.com/images/M/MV5BNWJlNzUzNGMtYTAwMS00ZjI2LWFmNWQtODcxNWUxODA5YmU1XkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_QL75_UX380_CR0,15,380,562_.jpg|The+life,+success+and+troubles+of+Wolfgang+Amadeus+Mozart,+as+told+by+Antonio+Salieri,+the+contemporaneous+composer+who+was+insanely+jealous+of+Mozart's+talent+and+claimed+to+have+murdered+him.|https://www.youtube.com/embed/r7kWQj9FCGY|Milos+Forman|Peter+Shaffer+(original+stage+play)|Zdenek+Mahler+(uncredited)|tt0086879|Inglourious+Basterds|https://m.media-amazon.com/images/M/MV5BOTJiNDEzOWYtMTVjOC00ZjlmLWE0NGMtZmE1OWVmZDQ2OWJhXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_UY67_CR0,0,45,67_AL_.jpg|top73|https://m.media-amazon.com/images/M/MV5BOTJiNDEzOWYtMTVjOC00ZjlmLWE0NGMtZmE1OWVmZDQ2OWJhXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_QL75_UX380_CR0,0,380,562_.jpg|In+Nazi-occupied+France+during+World+War+II,+a+plan+to+assassinate+Nazi+leaders+by+a+group+of+Jewish+U.S.+soldiers+coincides+with+a+theatre+owner's+vengeful+plans+for+the+same.|https://www.youtube.com/embed/KnrRy6kSFF0|Quentin+Tarantino|Quentin+Tarantino|tt0361748|Toy+Story|https://m.media-amazon.com/images/M/MV5BMDU2ZWJlMjktMTRhMy00ZTA5LWEzNDgtYmNmZTEwZTViZWJkXkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_UX45_CR0,0,45,67_AL_.jpg|top74|https://m.media-amazon.com/images/M/MV5BMDU2ZWJlMjktMTRhMy00ZTA5LWEzNDgtYmNmZTEwZTViZWJkXkEyXkFqcGdeQXVyNDQ2OTk4MzI@._V1_QL75_UX380_CR0,2,380,562_.jpg|A+cowboy+doll+is+profoundly+threatened+and+jealous+when+a+new+spaceman+action+figure+supplants+him+as+top+toy+in+a+boy's+bedroom.|https://www.youtube.com/embed/v-PjgYDrg70|John+Lasseter|John+Lasseter+(original+story+by)|Pete+Docter+(original+story+by)|Andrew+Stanton+(original+story+by)|tt0114709|Coco|https://m.media-amazon.com/images/M/MV5BYjQ5NjM0Y2YtNjZkNC00ZDhkLWJjMWItN2QyNzFkMDE3ZjAxXkEyXkFqcGdeQXVyODIxMzk5NjA@._V1_UY67_CR1,0,45,67_AL_.jpg|top75|https://m.media-amazon.com/images/M/MV5BYjQ5NjM0Y2YtNjZkNC00ZDhkLWJjMWItN2QyNzFkMDE3ZjAxXkEyXkFqcGdeQXVyODIxMzk5NjA@._V1_QL75_UY562_CR7,0,380,562_.jpg|Aspiring+musician+Miguel,+confronted+with+his+family's+ancestral+ban+on+music,+enters+the+Land+of+the+Dead+to+find+his+great-great-grandfather,+a+legendary+singer.|https://www.youtube.com/embed/Ga6RYejo6Hk|Lee+Unkrich|Adrian+Molina+(co-directed+by)|Lee+Unkrich+(original+story+by)|Jason+Katz+(original+story+by)|Matthew+Aldrich+(original+story+by)|tt2380307|Braveheart|https://m.media-amazon.com/images/M/MV5BMzkzMmU0YTYtOWM3My00YzBmLWI0YzctOGYyNTkwMWE5MTJkXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR0,0,45,67_AL_.jpg|top76|https://m.media-amazon.com/images/M/MV5BMzkzMmU0YTYtOWM3My00YzBmLWI0YzctOGYyNTkwMWE5MTJkXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UY562_CR1,0,380,562_.jpg|Scottish+warrior+William+Wallace+leads+his+countrymen+in+a+rebellion+to+free+his+homeland+from+the+tyranny+of+King+Edward+I+of+England.|https://www.youtube.com/embed/1NJO0jxBtMo|Mel+Gibson|Randall+Wallace|tt0112573|The+Boat|https://m.media-amazon.com/images/M/MV5BNDBjMWUxNTUtNjZiNi00YzJhLTgzNzUtMTRiY2FkZmMzYTNjXkEyXkFqcGdeQXVyMTUzMDUzNTI3._V1_UX45_CR0,0,45,67_AL_.jpg|top77|https://m.media-amazon.com/images/M/MV5BNDBjMWUxNTUtNjZiNi00YzJhLTgzNzUtMTRiY2FkZmMzYTNjXkEyXkFqcGdeQXVyMTUzMDUzNTI3._V1_QL75_UX380_CR0,7,380,562_.jpg|A+German+U-boat+stalks+the+frigid+waters+of+the+North+Atlantic+as+its+young+crew+experience+the+sheer+terror+and+claustrophobic+world+of+a+submariner+in+World+War+II.|https://www.youtube.com/embed/KhNnU4h1Q50|Wolfgang+Petersen|Wolfgang+Petersen|Lothar+G.+Buchheim+(based+on+the+novel+by)|tt0082096|Avengers:+Endgame|https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_UY67_CR0,0,45,67_AL_.jpg|top78|https://m.media-amazon.com/images/M/MV5BMTc5MDE2ODcwNV5BMl5BanBnXkFtZTgwMzI2NzQ2NzM@._V1_QL75_UX380_CR0,0,380,562_.jpg|After+the+devastating+events+of+Avengers:+Infinity+War+(2018),+the+universe+is+in+ruins.+With+the+help+of+remaining+allies,+the+Avengers+assemble+once+more+in+order+to+reverse+Thanos'+actions+and+restore+balance+to+the+universe.|https://www.youtube.com/embed/TcMBFSGVi1c|Anthony+Russo|Joe+Russo|Christopher+Markus+(screenplay+by)|Stephen+McFeely+(screenplay+by)|Stan+Lee+(based+on+the+Marvel+comics+by)|tt4154796|Princess+Mononoke|https://m.media-amazon.com/images/M/MV5BNGIzY2IzODQtNThmMi00ZDE4LWI5YzAtNzNlZTM1ZjYyYjUyXkEyXkFqcGdeQXVyODEzNjM5OTQ@._V1_UX45_CR0,0,45,67_AL_.jpg|top79|https://m.media-amazon.com/images/M/MV5BNGIzY2IzODQtNThmMi00ZDE4LWI5YzAtNzNlZTM1ZjYyYjUyXkEyXkFqcGdeQXVyODEzNjM5OTQ@._V1_QL75_UX380_CR0,4,380,562_.jpg|On+a+journey+to+find+the+cure+for+a+Tatarigami's+curse,+Ashitaka+finds+himself+in+the+middle+of+a+war+between+the+forest+gods+and+Tatara,+a+mining+colony.+In+this+quest+he+also+meets+San,+the+Mononoke+Hime.|https://www.youtube.com/embed/4OiMOHRDs14|Hayao+Miyazaki|Hayao+Miyazaki+(original+story)|Neil+Gaiman+(adapted+by:+English+version)|tt0119698|Top+Gun:+Maverick|https://m.media-amazon.com/images/M/MV5BZWYzOGEwNTgtNWU3NS00ZTQ0LWJkODUtMmVhMjIwMjA1ZmQwXkEyXkFqcGdeQXVyMjkwOTAyMDU@._V1_UY67_CR0,0,45,67_AL_.jpg|top80|https://m.media-amazon.com/images/M/MV5BZWYzOGEwNTgtNWU3NS00ZTQ0LWJkODUtMmVhMjIwMjA1ZmQwXkEyXkFqcGdeQXVyMjkwOTAyMDU@._V1_QL75_UX380_CR0,0,380,562_.jpg|After+thirty+years,+Maverick+is+still+pushing+the+envelope+as+a+top+naval+aviator,+but+must+confront+ghosts+of+his+past+when+he+leads+TOP+GUN's+elite+graduates+on+a+mission+that+demands+the+ultimate+sacrifice+from+those+chosen+to+fly+it.|https://www.youtube.com/embed/giXco2jaZ_4|Joseph+Kosinski|Jim+Cash+(based+on+characters+created+by)|Jack+Epps+Jr.+(based+on+characters+created+by)|Peter+Craig+(story+by)|tt1745960|Once+Upon+a+Time+in+America|https://m.media-amazon.com/images/M/MV5BMGFkNWI4MTMtNGQ0OC00MWVmLTk3MTktOGYxN2Y2YWVkZWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|top81|https://m.media-amazon.com/images/M/MV5BMGFkNWI4MTMtNGQ0OC00MWVmLTk3MTktOGYxN2Y2YWVkZWE2XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,16,380,562_.jpg|A+former+Prohibition-era+Jewish+gangster+returns+to+the+Lower+East+Side+of+Manhattan+35+years+later,+where+he+must+once+again+confront+the+ghosts+and+regrets+of+his+old+life.|https://www.youtube.com/embed/LcpCRyNo8T8|Sergio+Leone|Harry+Grey+(based+on+novel+"The+Hoods"+by)|Leonardo+Benvenuti+(screenplay+by)|Piero+De+Bernardi+(screenplay+by)|tt0087843|Good+Will+Hunting|https://m.media-amazon.com/images/M/MV5BOTI0MzcxMTYtZDVkMy00NjY1LTgyMTYtZmUxN2M3NmQ2NWJhXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX45_CR0,0,45,67_AL_.jpg|top82|https://m.media-amazon.com/images/M/MV5BOTI0MzcxMTYtZDVkMy00NjY1LTgyMTYtZmUxN2M3NmQ2NWJhXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UX380_CR0,9,380,562_.jpg|Will+Hunting,+a+janitor+at+M.I.T.,+has+a+gift+for+mathematics,+but+needs+help+from+a+psychologist+to+find+direction+in+his+life.|https://www.youtube.com/embed/PaZVjZEFkRs|Gus+Van+Sant|Matt+Damon|Ben+Affleck|tt0119217|Your+Name.|https://m.media-amazon.com/images/M/MV5BODRmZDVmNzUtZDA4ZC00NjhkLWI2M2UtN2M0ZDIzNDcxYThjL2ltYWdlXkEyXkFqcGdeQXVyNTk0MzMzODA@._V1_UY67_CR0,0,45,67_AL_.jpg|top83|https://m.media-amazon.com/images/M/MV5BODRmZDVmNzUtZDA4ZC00NjhkLWI2M2UtN2M0ZDIzNDcxYThjL2ltYWdlXkEyXkFqcGdeQXVyNTk0MzMzODA@._V1_QL75_UX380_CR0,0,380,562_.jpg|Two+strangers+find+themselves+linked+in+a+bizarre+way.+When+a+connection+forms,+will+distance+be+the+only+thing+to+keep+them+apart?|https://www.youtube.com/embed/xU47nhruN-Q|Makoto+Shinkai|Makoto+Shinkai+(based+on+his+novel)|Clark+Cheng+(english+script)|tt5311514|Requiem+for+a+Dream|https://m.media-amazon.com/images/M/MV5BOTdiNzJlOWUtNWMwNS00NmFlLWI0YTEtZmI3YjIzZWUyY2Y3XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX45_CR0,0,45,67_AL_.jpg|top84|https://m.media-amazon.com/images/M/MV5BOTdiNzJlOWUtNWMwNS00NmFlLWI0YTEtZmI3YjIzZWUyY2Y3XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UX380_CR0,4,380,562_.jpg|The+drug-induced+utopias+of+four+Coney+Island+people+are+shattered+when+their+addictions+run+deep.|https://www.youtube.com/embed/QBwzN4v1vA0|Darren+Aronofsky|Hubert+Selby+Jr.+(based+on+the+book+by)|Darren+Aronofsky+(screenplay)|tt0180093|Toy+Story+3|https://m.media-amazon.com/images/M/MV5BMTgxOTY4Mjc0MF5BMl5BanBnXkFtZTcwNTA4MDQyMw@@._V1_UY67_CR1,0,45,67_AL_.jpg|top85|https://m.media-amazon.com/images/M/MV5BMTgxOTY4Mjc0MF5BMl5BanBnXkFtZTcwNTA4MDQyMw@@._V1_QL75_UY562_CR9,0,380,562_.jpg|The+toys+are+mistakenly+delivered+to+a+day-care+center+instead+of+the+attic+right+before+Andy+leaves+for+college,+and+it's+up+to+Woody+to+convince+the+other+toys+that+they+weren't+abandoned+and+to+return+home.|https://www.youtube.com/embed/JcpWXaA2qeg|Lee+Unkrich|John+Lasseter+(story+by)|Andrew+Stanton+(story+by)|Lee+Unkrich+(story+by)|tt0435761|Singin'+in+the+Rain|https://m.media-amazon.com/images/M/MV5BZDRjNGViMjQtOThlMi00MTA3LThkYzQtNzJkYjBkMGE0YzE1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_UY67_CR0,0,45,67_AL_.jpg|top86|https://m.media-amazon.com/images/M/MV5BZDRjNGViMjQtOThlMi00MTA3LThkYzQtNzJkYjBkMGE0YzE1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_QL75_UY562_CR3,0,380,562_.jpg|A+silent+film+star+falls+for+a+chorus+girl+just+as+he+and+his+delusionally+jealous+screen+partner+are+trying+to+make+the+difficult+transition+to+talking+pictures+in+1920s+Hollywood.|https://www.youtube.com/embed/5_EVHeNEIJY|Musical|Stanley+Donen|Gene+Kelly|Betty+Comden+(story+by)|Adolph+Green+(story+by)|tt0045152|3+Idiots|https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY67_CR1,0,45,67_AL_.jpg|top87|https://m.media-amazon.com/images/M/MV5BNTkyOGVjMGEtNmQzZi00NzFlLTlhOWQtODYyMDc2ZGJmYzFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_QL75_UY562_CR5,0,380,562_.jpg|Two+friends+are+searching+for+their+long+lost+companion.+They+revisit+their+college+days+and+recall+the+memories+of+their+friend+who+inspired+them+to+think+differently,+even+as+the+rest+of+the+world+called+them+"idiots".|https://www.youtube.com/embed/xvszmNXdM4w|Rajkumar+Hirani|Rajkumar+Hirani+(screenplay)|Abhijat+Joshi+(screenplay)|Vidhu+Vinod+Chopra+(screenplay+associate)|tt1187043|High+and+Low|https://m.media-amazon.com/images/M/MV5BZGQ1NWZlYjctNDJmOS00MmU2LTg1ODItNmZiM2ZkNzJhNDI0XkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UY67_CR1,0,45,67_AL_.jpg|top88|https://m.media-amazon.com/images/M/MV5BZGQ1NWZlYjctNDJmOS00MmU2LTg1ODItNmZiM2ZkNzJhNDI0XkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_QL75_UY562_CR9,0,380,562_.jpg|An+executive+of+a+Yokohama+shoe+company+becomes+a+victim+of+extortion+when+his+chauffeur's+son+is+kidnapped+by+mistake+and+held+for+ransom.|https://www.youtube.com/embed/LV3z2Ytxu90|Akira+Kurosawa|Hideo+Oguni+(screenplay)|Ryûzô+Kikushima+(screenplay)|Eijirô+Hisaita+(screenplay)|tt0057565|Star+Wars:+Episode+VI+-+Return+of+the+Jedi|https://m.media-amazon.com/images/M/MV5BOWZlMjFiYzgtMTUzNC00Y2IzLTk1NTMtZmNhMTczNTk0ODk1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_UX45_CR0,0,45,67_AL_.jpg|top89|https://m.media-amazon.com/images/M/MV5BOWZlMjFiYzgtMTUzNC00Y2IzLTk1NTMtZmNhMTczNTk0ODk1XkEyXkFqcGdeQXVyNTAyODkwOQ@@._V1_QL75_UX380_CR0,13,380,562_.jpg|After+a+daring+mission+to+rescue+Han+Solo+from+Jabba+the+Hutt,+the+Rebels+dispatch+to+Endor+to+destroy+the+second+Death+Star.+Meanwhile,+Luke+struggles+to+help+Darth+Vader+back+from+the+dark+side+without+falling+into+the+Emperor's+trap.|https://www.youtube.com/embed/p4vIFhk621Q|Richard+Marquand|Lawrence+Kasdan+(screenplay+by)|George+Lucas+(screenplay+by)|tt0086190|2001:+A+Space+Odyssey|https://m.media-amazon.com/images/M/MV5BMmNlYzRiNDctZWNhMi00MzI4LThkZTctMTUzMmZkMmFmNThmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY67_CR0,0,45,67_AL_.jpg|top90|https://m.media-amazon.com/images/M/MV5BMmNlYzRiNDctZWNhMi00MzI4LThkZTctMTUzMmZkMmFmNThmXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,0,380,562_.jpg|After+uncovering+a+mysterious+artifact+buried+beneath+the+Lunar+surface,+a+spacecraft+is+sent+to+Jupiter+to+find+its+origins+-+a+spacecraft+manned+by+two+men+and+the+supercomputer+H.A.L.+9000.|https://www.youtube.com/embed/UgGCScAV7qU|Stanley+Kubrick|Stanley+Kubrick+(screenplay+by)|Arthur+C.+Clarke+(screenplay+by)|tt0062622|Eternal+Sunshine+of+the+Spotless+Mind|https://m.media-amazon.com/images/M/MV5BMTY4NzcwODg3Nl5BMl5BanBnXkFtZTcwNTEwOTMyMw@@._V1_UY67_CR0,0,45,67_AL_.jpg|top91|https://m.media-amazon.com/images/M/MV5BMTY4NzcwODg3Nl5BMl5BanBnXkFtZTcwNTEwOTMyMw@@._V1_QL75_UX380_CR0,0,380,562_.jpg|When+their+relationship+turns+sour,+a+couple+undergoes+a+medical+procedure+to+have+each+other+erased+from+their+memories.|https://www.youtube.com/embed/07-QBnEkgXU|Michel+Gondry|Charlie+Kaufman+(story)|Michel+Gondry+(story)|Pierre+Bismuth+(story)|tt0338013|Capernaum|https://m.media-amazon.com/images/M/MV5BY2Y3OWNkMTctYzNjYS00NWVkLTg4OWEtY2YxN2I3NDhlYzE0XkEyXkFqcGdeQXVyMTI3ODAyMzE2._V1_UY67_CR1,0,45,67_AL_.jpg|top92|https://m.media-amazon.com/images/M/MV5BY2Y3OWNkMTctYzNjYS00NWVkLTg4OWEtY2YxN2I3NDhlYzE0XkEyXkFqcGdeQXVyMTI3ODAyMzE2._V1_QL75_UY562_CR7,0,380,562_.jpg|While+serving+a+five-year+sentence+for+a+violent+crime,+a+12-year-old+boy+sues+his+parents+for+neglect.|https://www.youtube.com/embed/ULUo0048xZE|Nadine+Labaki|Nadine+Labaki+(screenplay)|Jihad+Hojeily+(screenplay)|Michelle+Keserwany+(screenplay)|tt8267604|Reservoir+Dogs|https://m.media-amazon.com/images/M/MV5BZmExNmEwYWItYmQzOS00YjA5LTk2MjktZjEyZDE1Y2QxNjA1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX45_CR0,0,45,67_AL_.jpg|top93|https://m.media-amazon.com/images/M/MV5BZmExNmEwYWItYmQzOS00YjA5LTk2MjktZjEyZDE1Y2QxNjA1XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_QL75_UX380_CR0,2,380,562_.jpg|When+a+simple+jewelry+heist+goes+horribly+wrong,+the+surviving+criminals+begin+to+suspect+that+one+of+them+is+a+police+informant.|https://www.youtube.com/embed/vayksn4Y93A|Quentin+Tarantino|Quentin+Tarantino+(background+radio+dialogue+written+by)|Roger+Avary+(background+radio+dialogue+written+by)|tt0105236|The+Hunt|https://m.media-amazon.com/images/M/MV5BMTg2NDg3ODg4NF5BMl5BanBnXkFtZTcwNzk3NTc3Nw@@._V1_UY67_CR1,0,45,67_AL_.jpg|top94|https://m.media-amazon.com/images/M/MV5BMTg2NDg3ODg4NF5BMl5BanBnXkFtZTcwNzk3NTc3Nw@@._V1_QL75_UY562_CR7,0,380,562_.jpg|A+teacher+lives+a+lonely+life,+all+the+while+struggling+over+his+son's+custody.+His+life+slowly+gets+better+as+he+finds+love+and+receives+good+news+from+his+son,+but+his+new+luck+is+about+to+be+brutally+shattered+by+an+innocent+little+lie.|https://www.youtube.com/embed/YsYn6L5fF9Q|Thomas+Vinterberg|Thomas+Vinterberg+(screenplay)|Tobias+Lindholm+(screenplay)|tt2106476|Citizen+Kane|https://m.media-amazon.com/images/M/MV5BYjBiOTYxZWItMzdiZi00NjlkLWIzZTYtYmFhZjhiMTljOTdkXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX45_CR0,0,45,67_AL_.jpg|top95|https://m.media-amazon.com/images/M/MV5BYjBiOTYxZWItMzdiZi00NjlkLWIzZTYtYmFhZjhiMTljOTdkXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_QL75_UX380_CR0,4,380,562_.jpg|Following+the+death+of+publishing+tycoon+Charles+Foster+Kane,+reporters+scramble+to+uncover+the+meaning+of+his+final+utterance:+'Rosebud.'|https://www.youtube.com/embed/8dxh3lwdOFw|Orson+Welles|Herman+J.+Mankiewicz+(original+screen+play)|Orson+Welles+(original+screen+play)|John+Houseman+(contributing+writer)|tt0033467|Lawrence+of+Arabia|https://m.media-amazon.com/images/M/MV5BYWY5ZjhjNGYtZmI2Ny00ODM0LWFkNzgtZmI1YzA2N2MxMzA0XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_UY67_CR0,0,45,67_AL_.jpg|top96|https://m.media-amazon.com/images/M/MV5BYWY5ZjhjNGYtZmI2Ny00ODM0LWFkNzgtZmI1YzA2N2MxMzA0XkEyXkFqcGdeQXVyNjUwNzk3NDc@._V1_QL75_UY562_CR5,0,380,562_.jpg|The+story+of+T.E.+Lawrence,+the+English+officer+who+successfully+united+and+led+the+diverse,+often+warring,+Arab+tribes+during+World+War+I+in+order+to+fight+the+Turks.|https://www.youtube.com/embed/vOlRhGEhG7k|David+Lean|Robert+Bolt+(screenplay+by)|Michael+Wilson+(screenplay+by)|tt0056172|M|https://m.media-amazon.com/images/M/MV5BODA4ODk3OTEzMF5BMl5BanBnXkFtZTgwMTQ2ODMwMzE@._V1_UX45_CR0,0,45,67_AL_.jpg|top97|https://m.media-amazon.com/images/M/MV5BODA4ODk3OTEzMF5BMl5BanBnXkFtZTgwMTQ2ODMwMzE@._V1_QL75_UX380_CR0,4,380,562_.jpg|When+the+police+in+a+German+city+are+unable+to+catch+a+child-murderer,+other+criminals+join+in+the+manhunt.|https://www.youtube.com/embed/nsVproWjN6c|Fritz+Lang|Thea+von+Harbou+(script)|Fritz+Lang+(script)|Egon+Jacobsohn+(article)|tt0022100|Come+and+See|https://m.media-amazon.com/images/M/MV5BNzU3OTI1MjItYTJiZC00NDI0LWFlNGYtOTQ1OTVhNDgwM2U3XkEyXkFqcGdeQXVyNTc2MDU0NDE@._V1_UY67_CR0,0,45,67_AL_.jpg|top98|https://m.media-amazon.com/images/M/MV5BNzU3OTI1MjItYTJiZC00NDI0LWFlNGYtOTQ1OTVhNDgwM2U3XkEyXkFqcGdeQXVyNTc2MDU0NDE@._V1_QL75_UX380_CR0,0,380,562_.jpg|After+finding+an+old+rifle,+a+young+boy+joins+the+Soviet+resistance+movement+against+ruthless+German+forces+and+experiences+the+horrors+of+World+War+II.|https://www.youtube.com/embed/UHaSQU-4wss|Elem+Klimov|Ales+Adamovich+(story)|Elem+Klimov+(screenplay)|tt0091251|North+by+Northwest|https://m.media-amazon.com/images/M/MV5BZDA3NDExMTUtMDlhOC00MmQ5LWExZGUtYmI1NGVlZWI4OWNiXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UX45_CR0,0,45,67_AL_.jpg|8.2|top99|https://m.media-amazon.com/images/M/MV5BZDA3NDExMTUtMDlhOC00MmQ5LWExZGUtYmI1NGVlZWI4OWNiXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_QL75_UX380_CR0,5,380,562_.jpg|A+New+York+City+advertising+executive+goes+on+the+run+after+being+mistaken+for+a+government+agent+by+a+group+of+foreign+spies,+and+falls+for+a+woman+whose+loyalties+he+begins+to+doubt.|https://www.youtube.com/embed/Fx0QuZJVTFE|Alfred+Hitchcock|Ernest+Lehman|tt0053125|Vertigo|https://m.media-amazon.com/images/M/MV5BYTE4ODEwZDUtNDFjOC00NjAxLWEzYTQtYTI1NGVmZmFlNjdiL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_UX45_CR0,0,45,67_AL_.jpg|top100|https://m.media-amazon.com/images/M/MV5BYTE4ODEwZDUtNDFjOC00NjAxLWEzYTQtYTI1NGVmZmFlNjdiL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjc1NTYyMjg@._V1_QL75_UX380_CR0,13,380,562_.jpg|A+former+San+Francisco+police+detective+juggles+wrestling+with+his+personal+demons+and+becoming+obsessed+with+the+hauntingly+beautiful+woman+he+has+been+hired+to+trail,+who+may+be+deeply+disturbed.|https://www.youtube.com/embed/Z5jvQwwHQNY|Alfred+Hitchcock|Alec+Coppel+(screenplay+by)|Samuel+A.+Taylor+(screenplay+by)|Pierre+Boileau+(based+on+the+novel+"D'Entre+Les+Morts"+by)|tt0052357^1|1JE|2|1IS|3|1JS|4|1IU|5|1ID|6|1JD|7|1JN|8|1JE|9|1JL|A|1IM|B|1JE|C|1JJ|D|1JU|E|1JM|F|1J0|G|1JJ|H|1JA|I|1IV|J|1JF|K|1IA|L|1I2|M|1JB|N|1JM|O|1JI|P|1JH|Q|1JY|R|1JJ|S|1IX|T|1JB|U|1J5|V|1JL|W|1IG|X|1JM|Y|1K3|Z|1JE|10|1JE|11|1JE|12|1JK|13|1JI|14|1JF|15|1JQ|16|1JY|17|1HY|18|1JV|19|1II|1A|1J8|1B|1HS|1C|1IO|1D|1IA|1E|1IZ|1F|1HN|1G|1J8|1H|1IZ|1I|1JK|1J|1J1|1K|1JW|1L|1JS|1M|1JQ|1N|1I6|1O|1ID|1P|1J0|1Q|1HW|1R|1K2|1S|1ID|1T|1J6|1U|1JJ|1V|1K2|1W|1IK|1X|1JW|1Y|1JN|1Z|1K3|20|1J4|21|1JT|22|1JF|23|1K1|24|1JF|25|1J1|26|1K3|27|1JH|28|1K6|29|1J4|2A|1JH|2B|1K0|2C|1JK|2D|1JU|2E|1I8|2F|1JT|2G|1IJ|2H|1J3|2I|1IO|2J|1JO|2K|1K2|2L|1JC|2M|1JW|2N|1HX|2O|1II|2P|1HN|2Q|1J5|2R|1IF|2S|1IE^^@$0|U0|1|2|3|4|5|6|7|8|9|U1|A|B|C|D|E|F|G|@H]|I|@J]|K|@L|M]|N|O]|$0|U2|1|P|3|Q|5|R|7|S|9|U3|A|T|C|U|E|V|G|@W|H]|I|@X]|K|@Y|Z]|N|10]|$0|U4|1|11|3|12|5|13|7|14|9|U5|A|15|C|16|E|17|G|@18|W|H]|I|@19]|K|@1A|1B|1C]|N|1D]|$0|U6|1|1E|3|1F|5|13|7|1G|9|U7|A|1H|C|1I|E|1J|G|@W|H]|I|@1K]|K|@1L|1M]|N|1N]|$0|U8|1|1O|3|1P|5|13|7|1Q|9|U9|A|1R|C|1S|E|1T|G|@W|H]|I|@1U]|K|@1V]|N|1W]|$0|UA|1|1X|3|1Y|5|1Z|7|20|9|UB|A|21|C|22|E|23|G|@24|H|25]|I|@26]|K|@27|28]|N|29]|$0|UC|1|2A|3|2B|5|1Z|7|2C|9|UD|A|2D|C|2E|E|2F|G|@18|2G|H]|I|@2H]|K|@2I|2J|2K]|N|2L]|$0|UE|1|2M|3|2N|5|2O|7|2P|9|UF|A|2Q|C|2R|E|2S|G|@W|H]|I|@2T]|K|@2U|2V]|N|2W]|$0|UG|1|2X|3|2Y|5|2O|7|2Z|9|UH|A|30|C|31|E|32|G|@18|2G|H]|I|@33]|K|@34|35|36]|N|37]|$0|UI|1|38|3|39|5|2O|7|3A|9|UJ|A|3B|C|3C|E|3D|G|@2G|3E]|I|@3F]|K|@3G|3H|3I]|N|3J]|$0|UK|1|3K|3|3L|5|2O|7|3M|9|UL|A|3N|C|3O|E|3P|G|@H|3Q]|I|@3R]|K|@3S|3T]|N|3U]|$0|UM|1|3V|3|3W|5|3X|7|3Y|9|UN|A|3Z|C|40|E|41|G|@H]|I|@42]|K|@43|44]|N|45]|$0|UO|1|46|3|47|5|3X|7|48|9|UP|A|49|C|4A|E|4B|G|@18|2G|4C]|I|@4D]|K|@4E]|N|4F]|$0|UQ|1|4G|3|4H|5|3X|7|4I|9|UR|A|4J|C|4K|E|4L|G|@18|2G|H]|I|@4M]|K|@4N|4O|4P]|N|4Q]|$0|US|1|4R|3|4S|5|3X|7|4T|9|UT|A|4U|C|4V|E|4W|G|@18|2G|4X]|I|@4Y]|K|@4Z|50|51]|N|52]|$0|UU|1|53|3|54|5|3X|7|55|9|UV|A|56|C|57|E|58|G|@18|4C]|I|@59|5A]|K|@5B|5C]|N|5D]|$0|UW|1|5E|3|5F|5|3X|7|5G|9|UX|A|5H|C|5I|E|5J|G|@24|W|H]|I|@5K]|K|@5L|5M]|N|5N]|$0|UY|1|5O|3|5P|5|5Q|7|5R|9|UZ|A|5S|C|5T|E|5U|G|@H]|I|@5V]|K|@5W|5X|5Y]|N|5Z]|$0|V0|1|60|3|61|5|5Q|7|62|9|V1|A|63|C|64|E|65|G|@W|H|66]|I|@67]|K|@68]|N|69]|$0|V2|1|6A|3|6B|5|5Q|7|6C|9|V3|A|6D|C|6E|E|6F|G|@18|H]|I|@6G]|K|@6H|6I|6J]|N|6K]|$0|V4|1|6L|3|6M|5|5Q|7|6N|9|V5|A|6O|C|6P|E|6Q|G|@H|6R|4X]|I|@6S]|K|@6T|6U|6V]|N|6W]|$0|V6|1|6X|3|6Y|5|5Q|7|6Z|9|V7|A|70|C|71|E|72|G|@W|H|73]|I|@74]|K|@75|76]|N|77]|$0|V8|1|78|3|79|5|5Q|7|7A|9|V9|A|7B|C|7C|E|7D|G|@W|H]|I|@7E|7F]|K|@7G|7H]|N|7I]|$0|VA|1|7J|3|7K|5|5Q|7|7L|9|VB|A|7M|C|7N|E|7O|G|@H|7P]|I|@7Q]|K|@7R]|N|7S]|$0|VC|1|7T|3|7U|5|5Q|7|7V|9|VD|A|7W|C|7X|E|7Y|G|@7Z|H|3Q]|I|@80]|K|@81|82]|N|83]|$0|VE|1|84|3|85|5|5Q|7|86|9|VF|A|87|C|88|E|89|G|@2G|H|4C]|I|@8A]|K|@8B|8C]|N|8D]|$0|VG|1|8E|3|8F|5|5Q|7|8G|9|VH|A|8H|C|8I|E|8J|G|@W|H|4X]|I|@8K]|K|@8L|8M]|N|8N]|$0|VI|1|8O|3|8P|5|5Q|7|8Q|9|VJ|A|8R|C|8S|E|8T|G|@18|2G|4X]|I|@8U]|K|@8V]|N|8W]|$0|VK|1|8X|3|8Y|5|8Z|7|90|9|VL|A|91|C|92|E|93|G|@18|4C]|I|@94]|K|@95|96]|N|97]|$0|VM|1|98|3|99|5|8Z|7|9A|9|VN|A|9B|C|9C|E|9D|G|@2G|7Z|4C]|I|@9E]|K|@9F|9G]|N|9H]|$0|VO|1|9I|3|9J|5|8Z|7|9K|9|VP|A|9L|C|9M|E|9N|G|@9O|2G|6R]|I|@9P]|K|@9Q]|N|9R]|$0|VQ|1|9S|3|9T|5|8Z|7|9U|9|VR|A|9V|C|9W|E|9X|G|@9Y|66|73]|I|@9Z]|K|@A0|A1]|N|A2]|$0|VS|1|A3|3|A4|5|8Z|7|A5|9|VT|A|A6|C|A7|E|A8|G|@24|H|A9]|I|@AA]|K|@AB|AC]|N|AD]|$0|VU|1|AE|3|AF|5|8Z|7|AG|9|VV|A|AH|C|AI|E|AJ|G|@H|73]|I|@AK]|K|@AL|AM]|N|AN]|$0|VW|1|AO|3|AP|5|8Z|7|AQ|9|VX|A|AR|C|AS|E|AT|G|@18|W|H]|I|@AU]|K|@AV]|N|AW]|$0|VY|1|AX|3|AY|5|8Z|7|AZ|9|VZ|A|B0|C|B1|E|B2|G|@9O|2G|H]|I|@B3|B4]|K|@B5|B6|B7]|N|B8]|$0|W0|1|B9|3|BA|5|8Z|7|BB|9|W1|A|BC|C|BD|E|BE|G|@18|2G|H]|I|@BF]|K|@BG|BH|BI]|N|BJ]|$0|W2|1|BK|3|BL|5|8Z|7|BM|9|W3|A|BN|C|BO|E|BP|G|@W|H]|I|@BQ]|K|@BR]|N|BS]|$0|W4|1|BT|3|BU|5|8Z|7|BV|9|W5|A|BW|C|BX|E|BY|G|@W|H|73]|I|@BZ]|K|@C0|C1|C2]|N|C3]|$0|W6|1|C4|3|C5|5|8Z|7|C6|9|W7|A|C7|C|C8|E|C9|G|@W|H|66]|I|@CA]|K|@CB]|N|CC]|$0|W8|1|CD|3|CE|5|8Z|7|CF|9|W9|A|CG|C|CH|E|CI|G|@H|66|4C]|I|@CJ]|K|@CK|CL|CM]|N|CN]|$0|WA|1|CO|3|CP|5|8Z|7|CQ|9|WB|A|CR|C|CS|E|CT|G|@H|A9]|I|@CU]|K|@CV]|N|CW]|$0|WC|1|CX|3|CY|5|8Z|7|CZ|9|WD|A|D0|C|D1|E|D2|G|@H|3Q|7P]|I|@D3]|K|@D4|D5|D6]|N|D7]|$0|WE|1|D8|3|D9|5|8Z|7|DA|9|WF|A|DB|C|DC|E|DD|G|@24|7Z|H]|I|@DE|DF]|K|@DG|DH|DI]|N|DJ]|$0|WG|1|DK|3|DL|5|8Z|7|DM|9|WH|A|DN|C|DO|E|DP|G|@18|H|66]|I|@DQ]|K|@DR|DS]|N|DT]|$0|WI|1|DU|3|DV|5|8Z|7|DW|9|WJ|A|DX|C|DY|E|DZ|G|@9O]|I|@E0]|K|@E1|E2]|N|E3]|$0|WK|1|E4|3|E5|5|E6|7|E7|9|WL|A|E8|C|E9|E|EA|G|@7Z|H|3Q]|I|@EB]|K|@EC]|N|ED]|$0|WM|1|EE|3|EF|5|E6|7|EG|9|WN|A|EH|C|EI|E|EJ|G|@3E]|I|@EK]|K|@EL|EM|EN]|N|EO]|$0|WO|1|EP|3|EQ|5|E6|7|ER|9|WP|A|ES|C|ET|E|EU|G|@66|73]|I|@EV]|K|@EW|EX]|N|EY]|$0|WQ|1|EZ|3|F0|5|E6|7|F1|9|WR|A|F2|C|F3|E|F4|G|@9Y|4C]|I|@F5]|K|@F6|F7]|N|F8]|$0|WS|1|F9|3|FA|5|E6|7|FB|9|WT|A|FC|C|FD|E|FE|G|@7Z|H|3Q]|I|@FF]|K|@FG|FH|FI]|N|FJ]|$0|WU|1|FK|3|FL|5|E6|7|FM|9|WV|A|FN|C|FO|E|FP|G|@H|3Q]|I|@FQ]|K|@FR|FS]|N|FT]|$0|WW|1|FU|3|FV|5|E6|7|FW|9|WX|A|FX|C|FY|E|FZ|G|@H|66|7P]|I|@G0]|K|@G1|G2|G3]|N|G4]|$0|WY|1|G5|3|G6|5|E6|7|G7|9|WZ|A|G8|C|G9|E|GA|G|@66|73]|I|@GB]|K|@GC|GD]|N|GE]|$0|X0|1|GF|3|GG|5|E6|7|GH|9|X1|A|GI|C|GJ|E|GK|G|@18|2G]|I|@GL]|K|@GM|GN|GO]|N|GP]|$0|X2|1|GQ|3|GR|5|E6|7|GS|9|X3|A|GT|C|GU|E|GV|G|@H|3E]|I|@GW]|K|@GX]|N|GY]|$0|X4|1|GZ|3|H0|5|E6|7|H1|9|X5|A|H2|C|H3|E|H4|G|@9O|2G|6R]|I|@H5]|K|@H6|H7|H8]|N|H9]|$0|X6|1|HA|3|HB|5|E6|7|HC|9|X7|A|HD|C|HE|E|HF|G|@H|66|73]|I|@HG]|K|@HH]|N|HI]|$0|X8|1|HJ|3|HK|5|E6|7|HL|9|X9|A|HM|C|HN|E|HO|G|@H|HP]|I|@HQ]|K|@HR|HS|HT]|N|HU]|$0|XA|1|HV|3|HW|5|E6|7|HX|9|XB|A|HY|C|HZ|E|I0|G|@H|7P]|I|@I1]|K|@I2|I3|I4]|N|I5]|$0|XC|1|I6|3|I7|5|E6|7|I8|9|XD|A|I9|C|IA|E|IB|G|@H|9Y]|I|@IC]|K|@ID|IE|IF]|N|IG]|$0|XE|1|IH|3|II|5|E6|7|IJ|9|XF|A|IK|C|IL|E|IM|G|@7Z|H|7P]|I|@IN]|K|@IO]|N|IP]|$0|XG|1|IQ|3|IR|5|E6|7|IS|9|XH|A|IT|C|IU|E|IV|G|@18|2G|4C]|I|@IW|IX]|K|@IY|IZ|J0]|N|J1]|$0|XI|1|J2|3|J3|5|E6|7|J4|9|XJ|A|J5|C|J6|E|J7|G|@W|H|66]|I|@J8]|K|@J9|JA|JB]|N|JC]|$0|XK|1|JD|3|JE|5|JF|7|JG|9|XL|A|JH|C|JI|E|JJ|G|@18|2G|4C]|I|@JK]|K|@JL|JM|JN]|N|JO]|$0|XM|1|JP|3|JQ|5|JF|7|JR|9|XN|A|JS|C|JT|E|JU|G|@H]|I|@JV]|K|@JW]|N|JX]|$0|XO|1|JY|3|JZ|5|JF|7|K0|9|XP|A|K1|C|K2|E|K3|G|@9O|18|2G]|I|@K4|K5|K6]|K|@K7|K8]|N|K9]|$0|XQ|1|KA|3|KB|5|JF|7|KC|9|XR|A|KD|C|KE|E|KF|G|@7Z|7P]|I|@KG]|K|@KH|KI|KJ]|N|KK]|$0|XS|1|KL|3|KM|5|JF|7|KN|9|XT|A|KO|C|KP|E|KQ|G|@18|H]|I|@KR]|K|@KS|KT|KU]|N|KV]|$0|XU|1|KW|3|KX|5|JF|7|KY|9|XV|A|KZ|C|L0|E|L1|G|@18|H|66]|I|@L2]|K|@L3|L4|L5|L6]|N|L7]|$0|XW|1|L8|3|L9|5|JF|7|LA|9|XX|A|LB|C|LC|E|LD|G|@W|H|73]|I|@LE]|K|@LF|LG|LH]|N|LI]|$0|XY|1|LJ|3|LK|5|JF|7|LL|9|XZ|A|LM|C|LN|E|LO|G|@24|H|A9]|I|@LP]|K|@LQ|LR]|N|LS]|$0|Y0|1|LT|3|LU|5|JF|7|LV|9|Y1|A|LW|C|LX|E|LY|G|@2G|H|7P]|I|@LZ]|K|@M0]|N|M1]|$0|Y2|1|M2|3|M3|5|JF|7|M4|9|Y3|A|M5|C|M6|E|M7|G|@9O|2G|7Z]|I|@M8]|K|@M9|MA|MB]|N|MC]|$0|Y4|1|MD|3|ME|5|JF|7|MF|9|Y5|A|MG|C|MH|E|MI|G|@9O|2G|7Z]|I|@MJ|MK]|K|@ML|MM|MN]|N|MO]|$0|Y6|1|MP|3|MQ|5|JF|7|MR|9|Y7|A|MS|C|MT|E|MU|G|@24|H|25]|I|@MV]|K|@MW]|N|MX]|$0|Y8|1|MY|3|MZ|5|JF|7|N0|9|Y9|A|N1|C|N2|E|N3|G|@H|7P]|I|@N4]|K|@N5|N6]|N|N7]|$0|YA|1|N8|3|N9|5|JF|7|NA|9|YB|A|NB|C|NC|E|ND|G|@18|2G|H]|I|@NE|NF]|K|@NG|NH|NI]|N|NJ]|$0|YC|1|NK|3|NL|5|JF|7|NM|9|YD|A|NN|C|NO|E|NP|G|@9O|18|2G]|I|@NQ]|K|@NR|NS]|N|NT]|$0|YE|1|NU|3|NV|5|JF|7|NW|9|YF|A|NX|C|NY|E|NZ|G|@18|H]|I|@O0]|K|@O1|O2|O3]|N|O4]|$0|YG|1|O5|3|O6|5|JF|7|O7|9|YH|A|O8|C|O9|E|OA|G|@W|H]|I|@OB]|K|@OC|OD|OE]|N|OF]|$0|YI|1|OG|3|OH|5|JF|7|OI|9|YJ|A|OJ|C|OK|E|OL|G|@H|3Q]|I|@OM]|K|@ON|OO]|N|OP]|$0|YK|1|OQ|3|OR|5|JF|7|OS|9|YL|A|OT|C|OU|E|OV|G|@9O|H|4X]|I|@OW]|K|@OX|OY]|N|OZ]|$0|YM|1|P0|3|P1|5|JF|7|P2|9|YN|A|P3|C|P4|E|P5|G|@H]|I|@P6]|K|@P7|P8]|N|P9]|$0|YO|1|PA|3|PB|5|JF|7|PC|9|YP|A|PD|C|PE|E|PF|G|@9O|2G|7Z]|I|@PG]|K|@PH|PI|PJ]|N|PK]|$0|YQ|1|PL|3|PM|5|JF|7|PN|9|YR|A|PO|C|PP|E|PQ|G|@7Z|PR|3Q]|I|@PS|PT]|K|@PU|PV]|N|PW]|$0|YS|1|PX|3|PY|5|JF|7|PZ|9|YT|A|Q0|C|Q1|E|Q2|G|@7Z|H]|I|@Q3]|K|@Q4|Q5|Q6]|N|Q7]|$0|YU|1|Q8|3|Q9|5|JF|7|QA|9|YV|A|QB|C|QC|E|QD|G|@W|H|66]|I|@QE]|K|@QF|QG|QH]|N|QI]|$0|YW|1|QJ|3|QK|5|JF|7|QL|9|YX|A|QM|C|QN|E|QO|G|@18|2G|4X]|I|@QP]|K|@QQ|QR]|N|QS]|$0|YY|1|QT|3|QU|5|JF|7|QV|9|YZ|A|QW|C|QX|E|QY|G|@2G|4C]|I|@QZ]|K|@R0|R1]|N|R2]|$0|Z0|1|R3|3|R4|5|JF|7|R5|9|Z1|A|R6|C|R7|E|R8|G|@H|3Q|4C]|I|@R9]|K|@RA|RB|RC]|N|RD]|$0|Z2|1|RE|3|RF|5|JF|7|RG|9|Z3|A|RH|C|RI|E|RJ|G|@H]|I|@RK]|K|@RL|RM|RN]|N|RO]|$0|Z4|1|RP|3|RQ|5|JF|7|RR|9|Z5|A|RS|C|RT|E|RU|G|@W|73]|I|@RV]|K|@RW|RX]|N|RY]|$0|Z6|1|RZ|3|S0|5|JF|7|S1|9|Z7|A|S2|C|S3|E|S4|G|@H]|I|@S5]|K|@S6|S7]|N|S8]|$0|Z8|1|S9|3|SA|5|JF|7|SB|9|Z9|A|SC|C|SD|E|SE|G|@H|66]|I|@SF]|K|@SG|SH|SI]|N|SJ]|$0|ZA|1|SK|3|SL|5|JF|7|SM|9|ZB|A|SN|C|SO|E|SP|G|@2G|24|H]|I|@SQ]|K|@SR|SS]|N|ST]|$0|ZC|1|SU|3|SV|5|JF|7|SW|9|ZD|A|SX|C|SY|E|SZ|G|@W|66|73]|I|@T0]|K|@T1|T2|T3]|N|T4]|$0|ZE|1|T5|3|T6|5|JF|7|T7|9|ZF|A|T8|C|T9|E|TA|G|@H|73|7P]|I|@TB]|K|@TC|TD]|N|TE]|$0|ZG|1|TF|3|TG|5|TH|7|TI|9|ZH|A|TJ|C|TK|E|TL|G|@18|2G|66]|I|@TM]|K|@TN]|N|TO]|$0|ZI|1|TP|3|TQ|5|TH|7|TR|9|ZJ|A|TS|C|TT|E|TU|G|@66|3Q|73]|I|@TV]|K|@TW|TX|TY]|N|TZ]]`;

// const pelis = jsonpack.pack(pelisExtended);
const pelis = jsonpack.unpack(pelisCompressed);
// console.log(pelis);
