const stylesinline = `
  html {
    scroll-behavior: smooth;
  }

  * {
    box-sizing: border-box;
  }

  body {
    margin: 0;
    font-family: Arial, Helvetica, sans-serif;
  }

  .container {
    background-color: ${containerBackgroundColor};
    transition: all 0.5s;
  }

  .main-header {
    background-color: ${mainHeaderBackgroundColor};
    padding: 1px;
    text-align: center;
  }

  .header-menu {
    padding: 10px 16px;
    background: ${headerMenuBackgroundColor};
    z-index: 1;
  }

  .mini-header {
    background-color: #ddd;
    width: 100%;
    position: absolute;
    top: -42px;
    transition: all 0.5s;
    z-index: 1;
  }
  .mini-header-content {
    max-width: 1140px;
    margin-left: auto;
    margin-right: auto;
    /* padding: 16px; */
  }
  .mini-h2 {
    padding: 4px 0px 0px 0px;
    margin: 10px;
  }
  
  .sticky {
    position: fixed;
    top: 0px;
    width: 100%;
  }
  .sticky + .main-content {
    padding-top: 102px;
  }
  
  #veil {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0);
    opacity: 0.8;
    z-index: -1;
  }
  /* Style the footer */
  .footer {
    background-color: #f1f1f1;
    padding: 10px;
  }
  /* Styles the sidenav */
  .sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 2;
    top: 0;
    left: 0;
    background-color: #111;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
  }
  .sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 18px;
    color: #818181;
    display: block;
    transition: 0.3s;
  }
  .sidenav a:hover {
    color: #f1f1f1;
  }
  .sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
  }
  
  @media screen and (max-height: 450px) {
    .sidenav {
      padding-top: 15px;
    }
    .sidenav a {
      font-size: 18px;
    }
  }
  @keyframes fadeIn {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  @keyframes fadeOut {
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
  .section {
    scroll-margin-top: 120px;
    height: 866px;
    overflow: hidden;
  }

  article {
    height: 144px;
  }

  /* Style the top navigation bar */
  .topnav {
    overflow: hidden;
    background-color: #333;
    margin-left: auto;
    margin-right: auto;
    max-width: ${topnavWidth};
  }

  /* Style the topnav links */
  .topnav a {
    float: left;
    display: block;
    color: #f2f2f2;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
  }

  /* Change color on hover */
  .topnav a:hover {
    background-color: #ddd;
    color: black;
  }

  .hamburguer {
    cursor: pointer;
    float: left;
    display: block;
    text-align: center;
    padding: 9px 20px;
    text-decoration: none;
  }
  
  .hamburguer-black {
    cursor: pointer;
    float: left;
    display: block;
    text-align: center;
    padding: 0px 20px;
    text-decoration: none;
  }

  .hamburguer:hover {
    background-color: #ddd;
    color: black;
  }

  .hamburguer:hover .bar {
    background-color: black;
  }

  .hamburguer:hover .bar-dark {
    background-color: gray;
  }

  .hamburguer.active .bar:nth-child(1) {
    transform: translateY(8px) rotate(45deg);
  }

  .hamburguer.active .bar:nth-child(2) {
    opacity: 0;
  }

  .hamburguer.active .bar:nth-child(3) {
    transform: translateY(-8px) rotate(-45deg);
  }

  .bar {
    display: block;
    width: 25px;
    height: 3px;
    margin: 5px 0;
    transition: all 0.3s ease;
    background-color: #f2f2f2;
  }

  .bar-dark {
    display: block;
    width: 25px;
    height: 3px;
    margin: 5px 0;
    transition: all 0.3s ease;
    background-color: black;
  }

  /* Style the main-content */
  .main-content {
    margin-left: auto;
    margin-right: auto;
    max-width: ${mainContentWidth};
    line-height: 2em;
  }

  .left {
    grid-area: left;
    display: grid;
    align-items: start;
    padding: 20px;
    background-color: ${leftSidebarBackgroundColor};
    width: 245px;
    height: 5200px;
  }

  .center {
    display: grid;
    grid-area: center;
    align-items: start;
    text-align: justify;
    padding: 20px;
    width: 918px;
    height: 5200px;
  }


  .right {
    grid-area: right;
    display: grid;
    align-items: start;
    padding: 20px;
    background-color: ${rightSidebarBackgroundColor};
    width: 245px;
    height: 5200px;
  }

  .content {
    display: flex;
    flex-direction: column;
    margin: 0 auto;
    max-width: 1440px;
  }

  .right {
    display: none;
  }

  @media screen and (min-width: 769px) {
    .content {
      grid-gap: 1rem;
      display: grid;
      gap: 1rem;
      grid-template-areas: "left center";
      grid-template-columns: 245px 892px;
      width: 1179px;
      height: 5200px;
    }

    .left {
      width: 245px;
      height: 5200px;
    }

    .center {
      display: grid;
      align-items: start;
      padding-right: 20px;
      width: 892px;
      height: 5200px;
    }

    .right {
      display: none;
    }
  }

  @media screen and (min-width: 1200px) {
    .content {
      grid-gap: 1rem;
      display: grid;
      gap: 1rem;
      grid-template-areas: "left center right";
      grid-template-columns: ${widthLeftColumnContent} ${widthMainColumnContent} ${widthRightColumnContent};
      width: 1440px;
      height: 5200px;
    }

    .left {
      width: 245px;
      height: 5200px;
    }

    .center {
      display: grid;
      align-items: start;
      width: 918px;
      height: 5200px;
    }

    .right {
      display: block;
      width: 245px;
      height: 5200px;
    }

    basic-hero {
      width: 1440px;
      height: 580px;
    }
  }

  .movies {
    display: grid;
    grid-template-areas: "sentinel" "pages" "more";
    justify-items: center;
  }

  .pages {
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(202px, 1fr));
    grid-template-areas: "sentinel" "pages" "more";
    justify-items: center;
    gap: 16px;
    margin: 16px 0px;
    width: 100%;
  }

  .sentinel {
    grid-area: sentinel; 
  }

  .pages {
    grid-area: pages;
  }

  .more {
    grid-area: more;
  }

  .more {
    display: flex;
    justify-content: center;
  }
`;