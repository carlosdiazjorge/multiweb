
/**
 * Basic Thumb - Carlos Manuel Diaz Jorge
 * Vanilla Thumb box Web Component con Background Image con Lazy Load
 * Web Component sin frameworks con carga perezosa de imágenes de fondo, cargadas desde el css (background-image style)
 * https://lenguajejs.com/webcomponents/
 * https://web.dev/lazy-loading-images/
 */
class BasicThumb extends HTMLElement {
  /**
   * Constructor
   * Es llamado cuando el componente se inicializa por primera vez. 
   * Debe llamar a super() y puede establecer valores por defecto
   * o realizar otros procesos pre-renderizado.
   */
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.bgurl = '';
    this.width = '';
    this.height = '';
    this.aspectratio = '';
    this.preheader = '';
    this.header = '';
    this.subheader = '';
    this.text = '';
    this.trailer = '';
  }

  /**
   * Atributos del componente
   * Retorna un array de atributos que el navegador observará.
   */
  static get observedAttributes() {
    return ['bgurl', 'width', 'height', 'aspectratio', 'preheader', 'header', 'subheader', 'text', 'trailer'];
  }

  /**
   * Cuando cambia el valor de una propiedad
   * Llamado cuando un atributo observado ha cambiado.
   * Los atributos definidos en HTML pasan inmediantamente, pero además JavaScript puede modificarlos.
   * @param {String} property name
   * @param {any} oldValue valor anterior
   * @param {any} newValue nuevo valor
   * @returns 
   */
  attributeChangedCallback(property, oldValue, newValue) {
    if (oldValue === newValue) return;
    this[property] = newValue;
  }

  styles() {
    // document.adoptedStyleSheets = [...document.adoptedStyleSheets, css];
    this.css = `
      <style>
        * {
          box-sizing: border-box;
        }
        
        /* The thumbnail image */
        .lazy-background {
          display: flex;
          justify-content: center;
          align-items: center;
          width: ${this.width};
          height: ${this.height};
          background-color: lightgray;
          background-position: center;
          background-repeat: no-repeat;
          background-size: ${this.width} ${this.height};
          position: relative;
          aspect-ratio: ${this.aspectratio};
          border: 1px solid black;
          border-radius: 4px;
        }

        .lazy-background.visible {
          background-image: url("${this.bgurl}");
          width: ${this.width};
          height: ${this.height};
        }

        /* Place text in the middle of the image */
        .info {
          position: absolute;
          bottom: 20%;
          width: 90%;
          color: #333333;
          /* background-color: rgba(255,255,255,0.6); */
          padding: 1rem;
          max-width: 90%;
        }
        
        .info .pre-header {
          height: 32px;
          width: ${this.width};
          overflow: hidden;
        }
        
        .info .header {
          height: 32px;
          width: ${this.width};
          overflow: hidden;
          font-weight: bold;
          background-color: rgba(255,255,255,0.6);
        }
        
        .info .text {
          height: 64px;
          overflow: hidden;
        }
      </style>
    `;
  }

  /**
   * Imagenes de background con Lazy Loading con la clase .lazy-background dentro del web component
   * Este código debe ejecutarse despues de que exista this.shadowRoot (render)
   * https://web.dev/lazy-loading-images/
   */
  _initLazyLoadBg() {
    var lazyBackgrounds = [].slice.call(this.shadowRoot.querySelectorAll(".lazy-background"));
    if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
      let lazyBackgroundObserver = new IntersectionObserver(function (entries, observer) {
        entries.forEach(function (entry) {
          if (entry.isIntersecting) {
            entry.target.classList.add("visible");
            lazyBackgroundObserver.unobserve(entry.target);
          }
        });
      });

      lazyBackgrounds.forEach(function (lazyBackground) {
        lazyBackgroundObserver.observe(lazyBackground);
      });
    }
  }

  /**
   * Connect component
   * Llamado cada vez que el componente es añadido o eliminado al/del DOM.
   * Se ejecuta cada vez que se requiera renderizar.
   */
  connectedCallback() {
    this.styles();
    this.render();
  }

  /**
   * El navegador llama a este método cuando el elemento es eliminado del documento.
   * Puede ser llamado varias veces, si el elemento se añade o elimina varias veces.
   */
  disconnectedCallback() {
    console.log('disconnectedCallback()');
  }

  render() {
    this.shadowRoot.innerHTML = `
      ${this.css}
      <a target='_blank' href='${this.trailer}'>
        <div class="lazy-background">
          <div class="info">
            <div class="pre-header">${this.preheader}</div>
            <span class="header">${this.header}</span>
            <div class="sub-header">${this.subheader}</div>
          </div>
        </div>
      </a>
    `;
    this._initLazyLoadBg();
  }
}

customElements.define("basic-thumb", BasicThumb);