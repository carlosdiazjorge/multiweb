
/**
 * Basic Hero - Carlos Manuel Diaz Jorge
 * Vanilla Hero Web Component with Background Image with Lazy Load
 * Web Component sin frameworks con carga perezosa de imágenes de fondo, cargadas desde el css (background-image style)
 * https://lenguajejs.com/webcomponents/
 * https://web.dev/lazy-loading-images/
 */
class BasicHero extends HTMLElement {

  /**
   * Constructor
   * Es llamado cuando el componente se inicializa por primera vez. 
   * Debe llamar a super() y puede establecer valores por defecto
   * o realizar otros procesos pre-renderizado.
   */
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.bgurl = '';
    this.side = ''; // left|right
    this.preheader = '';
    this.header = '';
    this.text = '';
  }

  /**
   * Atributos del componente
   * Retorna un array de atributos que el navegador observará.
   */
  static get observedAttributes() {
    return ['bgurl', 'side', 'preheader', 'header', 'text'];
  }

  /**
   * Cuando cambia el valor de una propiedad
   * Llamado cuando un atributo observado ha cambiado.
   * Los atributos definidos en HTML pasan inmediantamente, pero además JavaScript puede modificarlos.
   * @param {String} property name
   * @param {any} oldValue valor anterior
   * @param {any} newValue nuevo valor
   * @returns 
   */
  attributeChangedCallback(property, oldValue, newValue) {
    if (oldValue === newValue) return;
    this[property] = newValue;
  }

  styles() {
    // document.adoptedStyleSheets = [...document.adoptedStyleSheets, css];
    this.css = `
      <style>
        * {
          box-sizing: border-box;
        }
        
        /* The hero image */
        .lazy-background {
          /* Use "linear-gradient" to add a darken background effect to the image (photographer.jpg). This will make the text easier to read */
          /* background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url("./../img/loading.webp"); */
          width: 1440px;
          height: 580px;
          background-position: center;
          background-repeat: no-repeat;
          background-size: 1440px 580px;
          position: relative;
          aspect-ratio: 1440 / 580;
        }

        .lazy-background.visible {
          background-image: linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url("${this.bgurl}");
          width: 1440px;
          height: 580px;
        }

        /* Place text in the middle of the image */
        .hero-text {
          position: absolute;
          top: 20%;
          /* text-align: center;
          /* transform: translate(-50%, -50%); */
          width: 600px;
          height: 325px;
          color: #333333;
          background-color: rgba(255,255,255,0.6);
          padding: 4rem;
          max-width: 600px;
        }
        
        .hero-text.left {
          left: 10%;
        }
        
        .hero-text.right {
          left: 50%;
        }
        
        .hero-text .pre-header {
          height: 32px;
          width: 472px;
          overflow: hidden;
        }
        
        .hero-text .header {
          height: 32px;
          width: 472px;
          overflow: hidden;
        }
        
        .hero-text .text {
          height: 64px;
          width: 472px;
          overflow: hidden;
        }
      </style>
    `;
  }

  /**
   * Lazy Loading Background Images with .lazy-background class inside web component
   * This code must be after this.shadowRoot exist (render)
   * https://web.dev/lazy-loading-images/
   */
  _initLazyLoadBg() {
    var lazyBackgrounds = [].slice.call(this.shadowRoot.querySelectorAll(".lazy-background"));
    if ("IntersectionObserver" in window && "IntersectionObserverEntry" in window && "intersectionRatio" in window.IntersectionObserverEntry.prototype) {
      let lazyBackgroundObserver = new IntersectionObserver(function (entries, observer) {
        entries.forEach(function (entry) {
          if (entry.isIntersecting) {
            entry.target.classList.add("visible");
            lazyBackgroundObserver.unobserve(entry.target);
          }
        });
      });

      lazyBackgrounds.forEach(function (lazyBackground) {
        lazyBackgroundObserver.observe(lazyBackground);
      });
    }
  }

  /**
   * Connect component
   * Llamado cada vez que el componente es añadido o eliminado al/del DOM.
   * Se ejecuta cada vez que se requiera renderizar.
   */
  connectedCallback() {
    this.styles();
    this.render();
  }

  /**
   * El navegador llama a este método cuando el elemento es eliminado del documento.
   * Puede ser llamado varias veces, si el elemento se añade o elimina varias veces.
   */
  disconnectedCallback() {
    console.log('disconnectedCallback()');
  }

  render() {
    this.shadowRoot.innerHTML = `
      ${this.css}
      <div class="lazy-background">
        <div class="hero-text ${this.side}">
          <div class="pre-header">${this.preheader}</div>
          <h3 class="header">${this.header}</h3>
          <div class="text">${this.text}</div>
        </div>
      </div>
    `;
    this._initLazyLoadBg();
  }
}

customElements.define("basic-hero", BasicHero);

