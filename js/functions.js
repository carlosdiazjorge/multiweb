var navOpen = false;

export function sliderSideNavOverlay(evt) {
  evt.preventDefault();
  if (navOpen) {
    document.getElementById("left-sidenav").style.width = "0";
    document.getElementById("veil").style.zIndex = "-1";
    navOpen = false;
  } else {
    document.getElementById("left-sidenav").style.width = "250px";
    document.getElementById("veil").style.zIndex = "1";
    navOpen = true;
  }
}

function sliderSideNavPush(evt) {
  evt.preventDefault();
  if (navOpen) {
    document.getElementById("left-sidenav").style.width = "0";
    document.getElementById("container").style.marginLeft = "0";
    document.getElementById("veil").style.zIndex = "-1";
    navOpen = false;
  } else {
    document.getElementById("left-sidenav").style.width = "250px";
    document.getElementById("container").style.marginLeft = "250px";
    document.getElementById("veil").style.zIndex = "1";
    navOpen = true;
  }
}

window.onscroll = function () { doOnScroll() };
function doOnScroll() {
  toggleMenuSticky();
  scrollMiniHeader();
}

// Cuando el usuario hace scroll, fija el menú
function toggleMenuSticky() {
  var header = document.getElementById("menu-subheader");
  var sticky = header.offsetTop;
  if (window.pageYOffset + 22 > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

// Cuando el usuario hace scroll, baja el mini header
function scrollMiniHeader() {
  if (window.scrollY === 0) {
    document.getElementById("mini-header").style.position = "absolute";
    document.getElementById("mini-header").style.top = "-42px";
  } else {
    
    if (window.scrollY >= 5) {
      document.getElementById("mini-header").style.position = "fixed";
      document.getElementById("mini-header").style.top = "0px";
    } else {
      document.getElementById("mini-header").style.position = "fixed";
      document.getElementById("mini-header").style.top = window.scrollY + "px";
    }
  }
}

function currentBreakPoint() {
  return window.innerWidth >= 640 ? 'desktop' : 'mobile';
}

function goto(evt, element, veil = false) {
  evt.preventDefault();
  if (element === 'top') {
    window.scroll({top: 0, left: 0, behavior: 'smooth'});
  } else {
    document.getElementById(element).scrollIntoView({behavior: 'smooth'});
  }
  if (veil) {
    sliderSideNavOverlay(new Event('click', {bubbles: true, cancelable: true, composed: false }));
  }

}