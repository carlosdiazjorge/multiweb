// *********************************************************
// Main Render

async function render() {
  setCurrentBase();
  setCsp();
  setTitle(titleContent);

  let stylesInline = `
    ${styles_inline(``, stylesinline)}
  `;
  window.document.getElementById("styles-inline").innerHTML = stylesInline;

  let body = `
    ${left_side_nav(``, menu_links)}
    ${await main_container(``, mainheader, miniheader, menu_links, ``)}
    ${veil(``, [])}
  `;
  window.document.getElementById("body").innerHTML = body;
  var numPage = 1;
  infiniteScroll();
}

render();

// *********************************************************
// Functions
var navOpen = false;
// Muestra u oculta el menú lateral superpuesto al contenido
function sliderSideNavOverlay(evt) {
  evt.preventDefault();
  if (navOpen) {
    document.getElementById("left-sidenav").style.width = "0";
    document.getElementById("veil").style.backgroundColor = "rgba(0, 0, 0, 0.0)";
    document.getElementById('left-sidenav').style.boxShadow = 'none';
    document.getElementById("veil").style.zIndex = "-1";
    navOpen = false;
  } else {
    document.getElementById("left-sidenav").style.width = "250px";
    document.getElementById("veil").style.backgroundColor = "rgba(0, 0, 0, 0.2)";
    document.getElementById('left-sidenav').style.boxShadow = '10px 5px 15px rgba(64, 64, 64, 0.35)';
    document.getElementById("veil").style.zIndex = "1";
    navOpen = true;
  }
}

// Muestra u oculta el menú lateral desplazando al contenido
function sliderSideNavPush(evt) {
  evt.preventDefault();
  if (navOpen) {
    document.getElementById("left-sidenav").style.width = "0";
    document.getElementById("main-container").style.marginLeft = "0";
    document.getElementById("veil").style.backgroundColor = "rgba(0, 0, 0, 0.0)";
    document.getElementById("veil").style.zIndex = "-1";
    navOpen = false;
  } else {
    document.getElementById("left-sidenav").style.width = "250px";
    document.getElementById("main-container").style.marginLeft = "250px";
    document.getElementById("veil").style.backgroundColor = "rgba(0, 0, 0, 0.8)";
    document.getElementById("veil").style.zIndex = "1";
    navOpen = true;
  }
}

// Control del scroll de la página para reducir/ampliar la cabecera
window.onscroll = function () { doOnScroll() };
function doOnScroll() {
  toggleMenuSticky();
  // scrollMiniHeader();
}

// Cuando el usuario hace scroll, fija el menú
function toggleMenuSticky() {
  if (showMenuMainContainer) {
    var header = document.getElementById("menu-subheader");
    var sticky = header.offsetTop;
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }
}

// Cuando el usuario hace scroll, baja el mini header
function scrollMiniHeader() {
  if (window.scrollY === 0) {
    document.getElementById("mini-header").style.position = "absolute";
    document.getElementById("mini-header").style.top = "-42px";
  } else {
    if (window.scrollY >= 5) {
      document.getElementById("mini-header").style.position = "fixed";
      document.getElementById("mini-header").style.top = "0px";
    } else {
      document.getElementById("mini-header").style.position = "fixed";
      document.getElementById("mini-header").style.top = window.scrollY + "px";
    }
  }
}

// Obtiene el breackpoint actual
function currentBreakPoint() {
  return window.innerWidth >= 640 ? 'desktop' : 'mobile';
}

// Navegación en la página. Sustituye a los anchor
function goto(evt, element) {
  evt.preventDefault();
  if (element === 'top') {
    window.scroll({ top: 0, left: 0, behavior: 'smooth' });
  } else {
    document.getElementById(element).scrollIntoView({ behavior: 'smooth' });
  }

  if (evt.target.parentNode.id === 'left-sidenav') {
    sliderSideNavOverlay(new Event('click', { bubbles: true, cancelable: true, composed: false }));
  }
}

window.addEventListener('DOMContentLoaded', (event) => {
  console.log('DOM fully loaded and parsed at ' + window.location.href);
});

// *********************************************************
// Templates

function setTitle(titleContent) {
  const title = document.createElement("title");
  title.innerHTML = titleContent;
  window.document.getElementById("head").prepend(title);
}

function setCurrentBase() {
  let url = window.location.href.slice(0, window.location.href.lastIndexOf('/') + 1);
  const base = document.createElement('base');
  base.href = url;
  window.document.getElementById('head').prepend(base);
}

function setCsp() {
  if (window.location.protocol === 'https:') {
    let csp = document.createElement('meta');
    csp.httpEquiv = 'Content-Security-Policy';
    csp.content = "default-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; child-src 'none'; connect-src 'self'; img-src 'self' https://m.media-amazon.com/ https://projects.gitlab.io";
    window.document.getElementById('head').prepend(csp);
  }
}

function styles_inline(strings, ...keys) {
  const estilos = keys[0];
  let html = `${strings}`;
  html += `  ${estilos}`;
  return html;
}

function left_side_nav(strings, ...keys) {
  const links = keys[0];
  let html = `${strings}`;
  html += `<div id="left-sidenav" class="sidenav">`;
  html += `  <a href="#top" onclick="sliderSideNavOverlay(event)" class="closebtn">\&times;</a>`;
  links.forEach(link => {
    html += `  <a href="${link.anchor}" onclick="${link.onClick}">${link.content}</a>`;
  });
  html += `</div>`;
  return html;
}

async function main_container(strings, ...keys) {
  const mainheader = keys[0];
  // const miniheader = keys[1];
  const menu_links = keys[2];
  const article = keys[3];
  let html = `${strings}`;
  html += `<div id="main-container" class="container">`;
  html += `${main_header(``, mainheader)}`;
  // html += `${mini_header(``, miniheader)}`;
  if (showMenuMainContainer) {
    html += `${header_menu(``, menu_links)}`;
  }
  html += `${await main_content(``, article)}`;
  html += `</div>`;
  return html;
}

function main_header(strings, ...keys) {
  const data = keys[0];
  let html = `${strings}`;
  html += `
  <header id="header" class="main-header">
    <h1>${data.title}</h1>
    <h2>${data.subtitle}</h2>
    <p>${data.slogan}</p>
  </header>`;
  return html;
}

function mini_header(strings, ...keys) {
  const data = keys[0];
  let html = `${strings}`;
  html += `
  <header id="mini-header" class="mini-header">
    <div class="mini-header-content">
  `;
  if (!showMenuMainContainer) {
    html += `  <div onclick="sliderSideNavOverlay(event)" class="hamburguer-black"><span class="bar-dark"></span><span class="bar-dark"></span><span class="bar-dark"></span></div>`;
  }
  html += `  <h3 class="mini-h2">${data.title}</h3>
    </div>
  </header>`;
  return html;
}

function header_menu(strings, ...keys) {
  const links = keys[0];
  let html = `${strings}`;
  html += `<div id="menu-subheader" class="header-menu">`;
  html += `  <nav id="menu" class="topnav">`;
  html += `    <div onclick="sliderSideNavOverlay(event)" class="hamburguer"><span class="bar"></span><span class="bar"></span><span class="bar"></span></div>`;
  links.forEach(element => {
    html += `    <a href="${element.anchor}" onclick="${element.onClick}">${element.content}</a>`;
  });
  html += `  </nav>`;
  html += `</div>`;
  return html;
}

async function main_content(strings, ...keys) {
  const content = keys[0];
  // let data = await callApi();
  let html = `${strings}`;
  html += `<main id="inicio" class="main-content">`;
  html += `  <basic-hero
                bgurl="img/contacta.webp"
                side="left"
                preheader="Compras"
                header="Ofrecemos máxima satisfacción"
                text="Ofrecemos máxima satisfacción desde cualquier canal. Aportamos calidad de servicio y total disponibilidad."
              ></basic-hero>`;
  html += `   <div id="infinite-scroll-container" class='movies'>`;
  html += `     <div id="sentinel" class="sentinel"></div>`;
  html += `     <div id="pages" class="pages"></div>`;
  html += `     <div id='more' class='more'></div>`;
  html += `   </div>`;
  // html += `  </div>`;
  // html += `  <div class="content">`;
  // html += `    <div class="left">Izquierda Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ducimus cum commodi temporibus tenetur sunt. Id maxime maiores, possimus laboriosam sed aperiam error! Corrupti omnis quam est. Esse corrupti in unde!</div>`;
  // html += `    <div class="center">${data.title} ${content.toString()}</div>`;
  // html += `    <div class="right">Derecha Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ducimus cum commodi temporibus tenetur sunt. Id maxime maiores, possimus laboriosam sed aperiam error! Corrupti omnis quam est. Esse corrupti in unde!</div>`;
  // html += `  </div>`;
  // html += `  <basic-hero 
  //               bgurl="img/home_fococliente3.webp"
  //               side="right"
  //               preheader="Ingeniería"
  //               header="Innovamos para adaptarnos"
  //               text="Ponemos la tecnología digital a tu servicio. Desarrollamos continuamente nuevas herramientas para hacerte la vida más fácil."
  //             ></basic-hero>`;
  // html += `  <basic-hero 
  //               bgurl="img/home_tecnologia.webp"
  //               side="right"
  //               preheader="Ingeniería"
  //               header="Innovamos para adaptarnos"
  //               text="Ponemos la tecnología digital a tu servicio. Desarrollamos continuamente nuevas herramientas para hacerte la vida más fácil."
  //             ></basic-hero>`;
  html += `</main>`;
  html += `<footer>footer</footer>`;
  return html;
}

function setPage(numPage) {
  let start = page * (numPage - 1);
  let end = page * numPage;
  const pages = window.document.getElementById('pages');
  pelis.slice(start, end).forEach((movie) => {
    const el = document.createElement("basic-thumb");
    el.bgurl = movie.image;
    el.width ='202px';
    el.height ='300px';
    el.aspectratio = '375 / 555';
    el.preheader = movie.year;
    el.header = movie.title;
    el.subheader = movie.director[0];
    el.trailer = movie.trailer;
    pages.appendChild(el);
  });
}

/**
 * Infinite Scroll para cargar más <basic-thumb> en la página.
 * Así la página no tiene más contenido que el que se ve inicialmente, y carga más según va haciendo scroll.
 * Es como lazy-load pero de contenido html.
 * https://javascript.plainenglish.io/how-to-implement-infinite-scrolling-with-intersection-observer-api-9f2bc9ad8662
 * https://web.dev/patterns/web-vitals-patterns/infinite-scroll/infinite-scroll/
 * https://dev.to/ekqt/why-you-need-to-start-using-intersection-observer-1b8l
 * https://developer.mozilla.org/es/docs/Web/API/Intersection_Observer_API
 * https://blog.webdevsimplified.com/2022-01/intersection-observer/
 */
function infiniteScroll() {
  const options = { rootMargin: '0px', threshold: 1.0 };
  const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting && entry.intersectionRatio >= 0.75) {
        setPage(numPage);
        numPage++;
      }
    });
  }, options);
  const target = window.document.getElementById("more");
  observer.observe(target);
}

async function callApi() {
  try {
    // const url = 'https://jsonplaceholder.typicode.com/todos/2';
    const url = 'https://imdb-top-100-movies.p.rapidapi.com/';
    const params = {
      method: 'GET',
      headers: {
        'X-RapidAPI-Key': 'fc1dd49b81msh782a1cf246606c3p13ffdbjsn48504b6179cc',
        'X-RapidAPI-Host': 'imdb-top-100-movies.p.rapidapi.com'
      }
    };
    // const params = { method: 'GET', headers: { 'Content-Type': 'application/json' }};
    const response = await fetch(url, params);
    const responseData = response.json();
    return responseData;
  } catch (error) {
    console.error(error);
  }
}

function veil(strings, ...keys) {
  let html = `${strings}`;
  html += `<div id="veil" onclick="sliderSideNavOverlay(event)"></div>`;
  return html;
}