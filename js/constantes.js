
// Literals
const titleContent = 'Multi Website - Plain HTML';

const mainheader = {
  title: 'Bed & Fast',
  subtitle: 'Lo que busco.',
  slogan: 'Busca, compara, y si encuentras algo mejor.. cómpralo.'
};

const miniheader = {
  title: 'Bed & Fast'
};

// Sizes
topnavWidth = '1440px';
mainContentWidth = '1440px';
widthLeftColumnContent = '245px';
widthMainColumnContent = '918px';
widthRightColumnContent = '245px';

// Colors
const containerBackgroundColor = 'Snow';
const mainHeaderBackgroundColor = '#FCF3CF';
const headerMenuBackgroundColor = '#FCF3CF';
const leftSidebarBackgroundColor = 'WhiteSmoke';
const rightSidebarBackgroundColor = 'WhiteSmoke';

// Show/Hide parts
showMenuMainContainer = true;

// Structure
const page = 12;

// Menu
const menu_links = [
  {
    onClick: "goto(event, 'top')",
    anchor: '#top',
    content: 'Bed & Fast'
  },
  {
    onClick: "goto(event, 'inicio')",
    anchor: '#inicio',
    content: 'Inicio'
  },
  {
    onClick: "goto(event, 'nosotros')",
    anchor: '#nosotros',
    content: 'Nosotros'
  },
  {
    onClick: "goto(event, 'servicios')",
    anchor: '#servicios',
    content: 'Servicios'
  },
  {
    onClick: "goto(event, 'clientes')",
    anchor: '#clientes',
    content: 'Clientes'
  },
  {
    onClick: "goto(event, 'contacto')",
    anchor: '#contacto',
    content: 'Contacto'
  },
  {
    onClick: "goto(event, 'informacion')",
    anchor: '#informacion',
    content: 'Información'
  },
];