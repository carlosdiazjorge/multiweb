const article = `
<section class="section" id="inicio">
  <h1 id="main">Inicio</h1>
  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam modi eos deserunt consectetur eaque ipsa
    molestiae consequuntur, repudiandae, odit possimus quas nesciunt ea unde quae! Ratione alias voluptas
    temporibus sunt?</p>
  <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam modi eos deserunt consectetur eaque ipsa
    molestiae consequuntur, repudiandae, odit possimus quas nesciunt ea unde quae! Ratione alias voluptas
    temporibus sunt?</p>
</section>
<section class="section" id="nosotros">
  <h2>Nosotros</h2>
  <article>
    <p>Nosotros Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto
      magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
  </article>
  <article>
    <p>Nosotros Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto
      magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
  </article>
  <article>
    <p>Nosotros Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto
      magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
  </article>
</section>
<section class="section" id="servicios">
  <h2>Servicios</h2>
  <article>
    <p>Servicios Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto
      magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
  </article>
  <article>
    <p>Servicios Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto
      magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
  </article>
  <article>
    <p>Servicios Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto
      magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Mollitia, repudiandae sunt iusto magnam porro
      dolorum iste ex soluta consequuntur. Libero magnam ea vero, quod vitae veritatis ab hic nobis doloremque?
    </p>
  </article>
</section>
<section class="section" id="clientes">
  <h2>Clientes</h2>
  <article>
    <p>Clientes Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit
      ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
  </article>
</section>
<section class="section" id="contacto">
  <h2>Contacto</h2>
  <article>
    <p>Contacto Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit
      ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
  </article>
  <article>
    <p>Contacto Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit
      ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
  </article>
  <article>
    <p>Contacto Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit
      ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
  </article>
</section>
<section class="section" id="informacion">
  <h2>Información</h2>
  <article>
    <p>Información Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit
      ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, suscipit fuga eius impedit ratione
      maxime quibusdam minima facere iusto officiis nostrum et! Iusto accusamus incidunt neque labore aperiam
      dolor nemo!</p>
  </article>
</section>
`;